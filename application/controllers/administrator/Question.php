<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Question extends MY_Controller {
            
            protected $view_path = 'administrator/question/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('question_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            $this->load->model('question_model'); 
$this->load->model('title_model'); 
$this->load->model('category_model'); 
$this->load->model('questionaire_model'); 
}
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Question', '/administrator/question/browse');
        $this->breadcrumbs->push('Add', '/administrator/question/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['question'] = array();
        $this->data['action'] = 'Add';
    
        $this->data['select_question'] = $this->question_model->select();
                    $this->data['select_title'] = $this->title_model->select();
                    $this->data['select_category'] = $this->category_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('title_id', 'title_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question_id', 'question_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question', 'question', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description_1', 'description_1', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description_2', 'description_2', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description_3', 'description_3', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question_option', 'question_option', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('score_option', 'score_option', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->question_model->set_id(set_value('id'));
            $this->question_model->set_title_id(set_value('title_id'));
            $this->question_model->set_question_id(set_value('question_id'));
            $this->question_model->set_question(set_value('question'));
            $this->question_model->set_description_1(set_value('description_1'));
            $this->question_model->set_description_2(set_value('description_2'));
            $this->question_model->set_description_3(set_value('description_3'));
            $this->question_model->set_question_option(set_value('question_option'));
            $this->question_model->set_score_option(set_value('score_option'));
            $this->question_model->set_created(set_value('created'));
            $this->question_model->set_updated(set_value('updated'));
            $this->question_model->set_status(set_value('status'));
            		
                    if ($this->question_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! question Added');
                            redirect('/administrator/question/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! question could not be added');
                    }
        }                        
        }
                
                function detail($question_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Question', '/administrator/question/browse');
        $this->breadcrumbs->push('Details', '/administrator/question/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['question'] = $this->question_model->get_question_by_id($question_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($question_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Question', '/administrator/question/browse');
        $this->breadcrumbs->push('Edit', '/administrator/question/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['question'] = $this->question_model->get_question_by_id($question_id);
        $this->data['action'] = 'Edit';
        

        $this->data['select_question'] = $this->question_model->select();
                    $this->data['select_title'] = $this->title_model->select();
                    $this->data['select_category'] = $this->category_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('title_id', 'title_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question_id', 'question_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question', 'question', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description_1', 'description_1', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description_2', 'description_2', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description_3', 'description_3', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question_option', 'question_option', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('score_option', 'score_option', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->question_model->set_id($question_id);
                                    $this->question_model->set_title_id(set_value('title_id'));
                                    $this->question_model->set_question_id(set_value('question_id'));
                                    $this->question_model->set_question(set_value('question'));
                                    $this->question_model->set_description_1(set_value('description_1'));
                                    $this->question_model->set_description_2(set_value('description_2'));
                                    $this->question_model->set_description_3(set_value('description_3'));
                                    $this->question_model->set_question_option(set_value('question_option'));
                                    $this->question_model->set_score_option(set_value('score_option'));
                                    $this->question_model->set_created(set_value('created'));
                                    $this->question_model->set_updated(set_value('updated'));
                                    $this->question_model->set_status(set_value('status'));
                                    		
                    if ($this->question_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! question Updated');
                            redirect('/administrator/question/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! question could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Question', '/administrator/question/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/question/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/question/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/question/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, title_id, question_id, question, description_1, description_2, description_3, question_option, score_option, created, updated, status')
                ->from('question')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($question_id) {
        $this->question_model->set_id($question_id);                 		
        if ($this->question_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! question Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! question could not be Deleted');
        }
        redirect('/administrator/question/browse');
        }
                    
        
            }