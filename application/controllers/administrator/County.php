<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class County extends MY_Controller {
            
            protected $view_path = 'administrator/county/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('county_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            }
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse County', '/administrator/county/browse');
        $this->breadcrumbs->push('Add', '/administrator/county/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['county'] = array();
        $this->data['action'] = 'Add';
    
        $this->form_validation->set_rules('name', 'name', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->county_model->set_id(set_value('id'));
            $this->county_model->set_name(set_value('name'));
            $this->county_model->set_created(set_value('created'));
            $this->county_model->set_updated(set_value('updated'));
            $this->county_model->set_status(set_value('status'));
            		
                    if ($this->county_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! county Added');
                            redirect('/administrator/county/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! county could not be added');
                    }
        }                        
        }
                
                function detail($county_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse County', '/administrator/county/browse');
        $this->breadcrumbs->push('Details', '/administrator/county/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['county'] = $this->county_model->get_county_by_id($county_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($county_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse County', '/administrator/county/browse');
        $this->breadcrumbs->push('Edit', '/administrator/county/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['county'] = $this->county_model->get_county_by_id($county_id);
        $this->data['action'] = 'Edit';
        

        $this->form_validation->set_rules('name', 'name', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->county_model->set_id($county_id);
                                    $this->county_model->set_name(set_value('name'));
                                    $this->county_model->set_created(set_value('created'));
                                    $this->county_model->set_updated(set_value('updated'));
                                    $this->county_model->set_status(set_value('status'));
                                    		
                    if ($this->county_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! county Updated');
                            redirect('/administrator/county/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! county could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse County', '/administrator/county/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/county/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/county/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/county/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, name, created, updated, status')
                ->from('county')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($county_id) {
        $this->county_model->set_id($county_id);                 		
        if ($this->county_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! county Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! county could not be Deleted');
        }
        redirect('/administrator/county/browse');
        }
                    
        
            }