<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Dashboard extends MY_Controller {
            
            protected $view_path = 'administrator/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            }
            
            

function index() {
$this->dashboard();
}

function dashboard() {
                $this->data['dashboard'] = array('answer','category','county','quarter','question','questionaire','response','title');
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();        

}
            }