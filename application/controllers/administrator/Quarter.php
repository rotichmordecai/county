<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Quarter extends MY_Controller {
            
            protected $view_path = 'administrator/quarter/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('quarter_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            }
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Quarter', '/administrator/quarter/browse');
        $this->breadcrumbs->push('Add', '/administrator/quarter/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['quarter'] = array();
        $this->data['action'] = 'Add';
    
        $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->quarter_model->set_id(set_value('id'));
            $this->quarter_model->set_title(set_value('title'));
            $this->quarter_model->set_created(set_value('created'));
            $this->quarter_model->set_updated(set_value('updated'));
            $this->quarter_model->set_status(set_value('status'));
            		
                    if ($this->quarter_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! quarter Added');
                            redirect('/administrator/quarter/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! quarter could not be added');
                    }
        }                        
        }
                
                function detail($quarter_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Quarter', '/administrator/quarter/browse');
        $this->breadcrumbs->push('Details', '/administrator/quarter/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['quarter'] = $this->quarter_model->get_quarter_by_id($quarter_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($quarter_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Quarter', '/administrator/quarter/browse');
        $this->breadcrumbs->push('Edit', '/administrator/quarter/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['quarter'] = $this->quarter_model->get_quarter_by_id($quarter_id);
        $this->data['action'] = 'Edit';
        

        $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->quarter_model->set_id($quarter_id);
                                    $this->quarter_model->set_title(set_value('title'));
                                    $this->quarter_model->set_created(set_value('created'));
                                    $this->quarter_model->set_updated(set_value('updated'));
                                    $this->quarter_model->set_status(set_value('status'));
                                    		
                    if ($this->quarter_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! quarter Updated');
                            redirect('/administrator/quarter/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! quarter could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Quarter', '/administrator/quarter/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/quarter/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/quarter/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/quarter/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, title, created, updated, status')
                ->from('quarter')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($quarter_id) {
        $this->quarter_model->set_id($quarter_id);                 		
        if ($this->quarter_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! quarter Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! quarter could not be Deleted');
        }
        redirect('/administrator/quarter/browse');
        }
                    
        
            }