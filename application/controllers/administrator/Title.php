<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Title extends MY_Controller {
            
            protected $view_path = 'administrator/title/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('title_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            $this->load->model('category_model'); 
$this->load->model('questionaire_model'); 
}
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Title', '/administrator/title/browse');
        $this->breadcrumbs->push('Add', '/administrator/title/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['title'] = array();
        $this->data['action'] = 'Add';
    
        $this->data['select_category'] = $this->category_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('category_id', 'category_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description', 'description', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->title_model->set_id(set_value('id'));
            $this->title_model->set_category_id(set_value('category_id'));
            $this->title_model->set_title(set_value('title'));
            $this->title_model->set_description(set_value('description'));
            $this->title_model->set_created(set_value('created'));
            $this->title_model->set_updated(set_value('updated'));
            $this->title_model->set_status(set_value('status'));
            		
                    if ($this->title_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! title Added');
                            redirect('/administrator/title/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! title could not be added');
                    }
        }                        
        }
                
                function detail($title_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Title', '/administrator/title/browse');
        $this->breadcrumbs->push('Details', '/administrator/title/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['title'] = $this->title_model->get_title_by_id($title_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($title_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Title', '/administrator/title/browse');
        $this->breadcrumbs->push('Edit', '/administrator/title/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['title'] = $this->title_model->get_title_by_id($title_id);
        $this->data['action'] = 'Edit';
        

        $this->data['select_category'] = $this->category_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('category_id', 'category_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('description', 'description', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->title_model->set_id($title_id);
                                    $this->title_model->set_category_id(set_value('category_id'));
                                    $this->title_model->set_title(set_value('title'));
                                    $this->title_model->set_description(set_value('description'));
                                    $this->title_model->set_created(set_value('created'));
                                    $this->title_model->set_updated(set_value('updated'));
                                    $this->title_model->set_status(set_value('status'));
                                    		
                    if ($this->title_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! title Updated');
                            redirect('/administrator/title/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! title could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Title', '/administrator/title/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/title/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/title/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/title/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, category_id, title, description, created, updated, status')
                ->from('title')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($title_id) {
        $this->title_model->set_id($title_id);                 		
        if ($this->title_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! title Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! title could not be Deleted');
        }
        redirect('/administrator/title/browse');
        }
                    
        
            }