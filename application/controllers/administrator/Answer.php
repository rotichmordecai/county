<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Answer extends MY_Controller {
            
            protected $view_path = 'administrator/answer/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('answer_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            $this->load->model('question_model'); 
$this->load->model('response_model'); 
$this->load->model('question_model'); 
$this->load->model('title_model'); 
$this->load->model('category_model'); 
$this->load->model('questionaire_model'); 
}
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Answer', '/administrator/answer/browse');
        $this->breadcrumbs->push('Add', '/administrator/answer/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['answer'] = array();
        $this->data['action'] = 'Add';
    
        $this->data['select_question'] = $this->question_model->select();
                    $this->data['select_response'] = $this->response_model->select();
                    $this->data['select_question'] = $this->question_model->select();
                    $this->data['select_title'] = $this->title_model->select();
                    $this->data['select_category'] = $this->category_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('response_id', 'response_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question_id', 'question_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('value', 'value', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->answer_model->set_id(set_value('id'));
            $this->answer_model->set_response_id(set_value('response_id'));
            $this->answer_model->set_question_id(set_value('question_id'));
            $this->answer_model->set_value(set_value('value'));
            $this->answer_model->set_created(set_value('created'));
            $this->answer_model->set_updated(set_value('updated'));
            $this->answer_model->set_status(set_value('status'));
            		
                    if ($this->answer_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! answer Added');
                            redirect('/administrator/answer/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! answer could not be added');
                    }
        }                        
        }
                
                function detail($answer_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Answer', '/administrator/answer/browse');
        $this->breadcrumbs->push('Details', '/administrator/answer/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['answer'] = $this->answer_model->get_answer_by_id($answer_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($answer_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Answer', '/administrator/answer/browse');
        $this->breadcrumbs->push('Edit', '/administrator/answer/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['answer'] = $this->answer_model->get_answer_by_id($answer_id);
        $this->data['action'] = 'Edit';
        

        $this->data['select_question'] = $this->question_model->select();
                    $this->data['select_response'] = $this->response_model->select();
                    $this->data['select_question'] = $this->question_model->select();
                    $this->data['select_title'] = $this->title_model->select();
                    $this->data['select_category'] = $this->category_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('response_id', 'response_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('question_id', 'question_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('value', 'value', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->answer_model->set_id($answer_id);
                                    $this->answer_model->set_response_id(set_value('response_id'));
                                    $this->answer_model->set_question_id(set_value('question_id'));
                                    $this->answer_model->set_value(set_value('value'));
                                    $this->answer_model->set_created(set_value('created'));
                                    $this->answer_model->set_updated(set_value('updated'));
                                    $this->answer_model->set_status(set_value('status'));
                                    		
                    if ($this->answer_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! answer Updated');
                            redirect('/administrator/answer/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! answer could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Answer', '/administrator/answer/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/answer/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/answer/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/answer/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, response_id, question_id, value, created, updated, status')
                ->from('answer')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($answer_id) {
        $this->answer_model->set_id($answer_id);                 		
        if ($this->answer_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! answer Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! answer could not be Deleted');
        }
        redirect('/administrator/answer/browse');
        }
                    
        
            }