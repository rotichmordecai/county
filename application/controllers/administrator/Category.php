<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Category extends MY_Controller {
            
            protected $view_path = 'administrator/category/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('category_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            $this->load->model('questionaire_model'); 
}
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Category', '/administrator/category/browse');
        $this->breadcrumbs->push('Add', '/administrator/category/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['category'] = array();
        $this->data['action'] = 'Add';
    
        $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('questionaire_id', 'questionaire_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->category_model->set_id(set_value('id'));
            $this->category_model->set_questionaire_id(set_value('questionaire_id'));
            $this->category_model->set_title(set_value('title'));
            $this->category_model->set_created(set_value('created'));
            $this->category_model->set_updated(set_value('updated'));
            $this->category_model->set_status(set_value('status'));
            		
                    if ($this->category_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! category Added');
                            redirect('/administrator/category/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! category could not be added');
                    }
        }                        
        }
                
                function detail($category_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Category', '/administrator/category/browse');
        $this->breadcrumbs->push('Details', '/administrator/category/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['category'] = $this->category_model->get_category_by_id($category_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($category_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Category', '/administrator/category/browse');
        $this->breadcrumbs->push('Edit', '/administrator/category/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['category'] = $this->category_model->get_category_by_id($category_id);
        $this->data['action'] = 'Edit';
        

        $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('questionaire_id', 'questionaire_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->category_model->set_id($category_id);
                                    $this->category_model->set_questionaire_id(set_value('questionaire_id'));
                                    $this->category_model->set_title(set_value('title'));
                                    $this->category_model->set_created(set_value('created'));
                                    $this->category_model->set_updated(set_value('updated'));
                                    $this->category_model->set_status(set_value('status'));
                                    		
                    if ($this->category_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! category Updated');
                            redirect('/administrator/category/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! category could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Category', '/administrator/category/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/category/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/category/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/category/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, questionaire_id, title, created, updated, status')
                ->from('category')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($category_id) {
        $this->category_model->set_id($category_id);                 		
        if ($this->category_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! category Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! category could not be Deleted');
        }
        redirect('/administrator/category/browse');
        }
                    
        
            }