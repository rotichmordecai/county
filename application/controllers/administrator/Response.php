<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Response extends MY_Controller {
            
            protected $view_path = 'administrator/response/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('response_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            $this->load->model('county_model'); 
$this->load->model('quarter_model'); 
$this->load->model('questionaire_model'); 
}
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Response', '/administrator/response/browse');
        $this->breadcrumbs->push('Add', '/administrator/response/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['response'] = array();
        $this->data['action'] = 'Add';
    
        $this->data['select_county'] = $this->county_model->select();
                    $this->data['select_quarter'] = $this->quarter_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('county_id', 'county_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('quarter_id', 'quarter_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('questionaire_id', 'questionaire_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('response', 'response', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->response_model->set_id(set_value('id'));
            $this->response_model->set_county_id(set_value('county_id'));
            $this->response_model->set_quarter_id(set_value('quarter_id'));
            $this->response_model->set_questionaire_id(set_value('questionaire_id'));
            $this->response_model->set_response(set_value('response'));
            $this->response_model->set_created(set_value('created'));
            $this->response_model->set_updated(set_value('updated'));
            $this->response_model->set_status(set_value('status'));
            		
                    if ($this->response_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! response Added');
                            redirect('/administrator/response/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! response could not be added');
                    }
        }                        
        }
                
                function detail($response_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Response', '/administrator/response/browse');
        $this->breadcrumbs->push('Details', '/administrator/response/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['response'] = $this->response_model->get_response_by_id($response_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($response_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Response', '/administrator/response/browse');
        $this->breadcrumbs->push('Edit', '/administrator/response/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['response'] = $this->response_model->get_response_by_id($response_id);
        $this->data['action'] = 'Edit';
        

        $this->data['select_county'] = $this->county_model->select();
                    $this->data['select_quarter'] = $this->quarter_model->select();
                    $this->data['select_questionaire'] = $this->questionaire_model->select();
                    $this->form_validation->set_rules('county_id', 'county_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('quarter_id', 'quarter_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('questionaire_id', 'questionaire_id', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('response', 'response', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->response_model->set_id($response_id);
                                    $this->response_model->set_county_id(set_value('county_id'));
                                    $this->response_model->set_quarter_id(set_value('quarter_id'));
                                    $this->response_model->set_questionaire_id(set_value('questionaire_id'));
                                    $this->response_model->set_response(set_value('response'));
                                    $this->response_model->set_created(set_value('created'));
                                    $this->response_model->set_updated(set_value('updated'));
                                    $this->response_model->set_status(set_value('status'));
                                    		
                    if ($this->response_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! response Updated');
                            redirect('/administrator/response/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! response could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Response', '/administrator/response/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/response/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/response/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/response/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, county_id, quarter_id, questionaire_id, response, created, updated, status')
                ->from('response')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($response_id) {
        $this->response_model->set_id($response_id);                 		
        if ($this->response_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! response Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! response could not be Deleted');
        }
        redirect('/administrator/response/browse');
        }
                    
        
            }