<?php

        defined('BASEPATH') OR exit('No direct script access allowed');
        
        class Questionaire extends MY_Controller {
            
            protected $view_path = 'administrator/questionaire/';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library('datatables');
                $this->load->library('template');
                $this->load->library('form_validation');
                $this->load->model('questionaire_model');
                $this->template->set_template('administrator');
                $this->initialize('administrator');
            }
            
            function index() {
            $this->browse();
        }    
        function add() {
            
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Questionaire', '/administrator/questionaire/browse');
        $this->breadcrumbs->push('Add', '/administrator/questionaire/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['questionaire'] = array();
        $this->data['action'] = 'Add';
    
        $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path .'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->questionaire_model->set_id(set_value('id'));
            $this->questionaire_model->set_title(set_value('title'));
            $this->questionaire_model->set_created(set_value('created'));
            $this->questionaire_model->set_updated(set_value('updated'));
            $this->questionaire_model->set_status(set_value('status'));
            		
                    if ($this->questionaire_model->insert() > 0) 
                    {
                            $this->message->set('success', 'Success! questionaire Added');
                            redirect('/administrator/questionaire/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! questionaire could not be added');
                    }
        }                        
        }
                
                function detail($questionaire_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Questionaire', '/administrator/questionaire/browse');
        $this->breadcrumbs->push('Details', '/administrator/questionaire/detail');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['questionaire'] = $this->questionaire_model->get_questionaire_by_id($questionaire_id);
                
$this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        
            }function edit($questionaire_id) {
        
        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
        $this->breadcrumbs->push('Browse Questionaire', '/administrator/questionaire/browse');
        $this->breadcrumbs->push('Edit', '/administrator/questionaire/edit');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['questionaire'] = $this->questionaire_model->get_questionaire_by_id($questionaire_id);
        $this->data['action'] = 'Edit';
        

        $this->form_validation->set_rules('title', 'title', 'trim|xss_clean');                      
                    $this->form_validation->set_rules('status', 'status', 'trim|xss_clean');                      
                    
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view('content', $this->view_path . 'form', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		$this->questionaire_model->set_id($questionaire_id);
                                    $this->questionaire_model->set_title(set_value('title'));
                                    $this->questionaire_model->set_created(set_value('created'));
                                    $this->questionaire_model->set_updated(set_value('updated'));
                                    $this->questionaire_model->set_status(set_value('status'));
                                    		
                    if ($this->questionaire_model->update() > 0) 
                    {
                            $this->message->set('success', 'Success! questionaire Updated');
                            redirect('/administrator/questionaire/browse');
                    }
                    else
                    {
                    $this->message->set('error', 'Error! questionaire could not be Updated');
                    }
            }
                }
        
        function browse() {
        
            $this->breadcrumbs->push('Home', '/');
            $this->breadcrumbs->push('Administrator', '/administrator/dashboard/index');
            $this->breadcrumbs->push('Browse Questionaire', '/administrator/questionaire/browse');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
	    $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        
        
        function browse_json() {
        
            $action = '<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/questionaire/detail/$1">View</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/questionaire/edit/$1">Edit</a>
                            <a class="btn btn-default" href="http://localhost/county/index.php/administrator/questionaire/delete/$1">Delete</a>
                        </div>';
                
	    $this->datatables
                ->select('id, title, created, updated, status')
                ->from('questionaire')
                ->add_column('action', $action, 'id');      
                
        header('Content-Type: application/json');
        echo $this->datatables->generate();
	}
                        
        
        
        function delete($questionaire_id) {
        $this->questionaire_model->set_id($questionaire_id);                 		
        if ($this->questionaire_model->delete() > 0) 
        {
            $this->message->set('success', 'Success! questionaire Deleted');
        }
        else
        {
            $this->message->set('error', 'Error! questionaire could not be Deleted');
        }
        redirect('/administrator/questionaire/browse');
        }
                    
        
            }