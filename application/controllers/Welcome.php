<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    protected $view_path = 'welcome/';
    protected $data = array();

    public function __construct() {

        parent::__construct();

        $this->load->library('datatables');
        $this->load->library('template');
        $this->load->library('form_validation');
        $this->load->model('question_model');
        $this->template->set_template('administrator');
        $this->initialize('administrator');
        $this->load->model('question_model');
        $this->load->model('title_model');
        $this->load->model('category_model');
        $this->load->model('questionaire_model');

        $this->load->model('question_model');

        $this->load->model('quarter_model');
        $this->load->model('county_model');

        $this->load->model('response_model');

        $this->load->helper('form');

        $this->load->library('county');
    }

    function index() {

        if (isset($_POST['submit'])) {

            $post = $_POST;
            unset($post['submit']);

            $this->response_model->set_county_id($post['county_id']);
            $this->response_model->set_quarter_id($post['quarter_id']);
            $this->response_model->set_questionaire_id($post['questionaire_id']);
            $this->response_model->set_response(json_encode($post));
            $this->response_model->set_status(1);

            if ($this->response_model->insert() > 0) {
                $this->message->set('success', 'Success! response saved');
                redirect(current_url());
            }
        } else {
            $this->data['questionaire'] = $this->questionaire_model->select();
            $this->data['county'] = $this->county_model->select();
            $this->data['quarter'] = $this->quarter_model->select();
            $this->data['question'] = $this->question_model->select();
            $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        }
    }

    function dashboard() {

        $response = $this->county->response();

        $this->data['count']['county_id'] = array();
        $this->data['count']['questionaire_id'] = array();
        $this->data['count']['quarter_id'] = array();

        foreach ($response as $value) {
            array_push($this->data['count']['county_id'], $value['detail']['county_id']);
            array_push($this->data['count']['questionaire_id'], $value['detail']['questionaire_id']);
            array_push($this->data['count']['quarter_id'], $value['detail']['quarter_id']);
        }
        
        $this->data['response'] = $response;
        
        $this->data['recent'] = $this->response_model->select();
        
        array_unique($this->data['count']['county_id']);
        array_unique($this->data['count']['questionaire_id']);
        array_unique($this->data['count']['quarter_id']);
        
        array_values($this->data['count']['county_id']);
        array_values($this->data['count']['questionaire_id']);
        array_values($this->data['count']['quarter_id']);
        
        $county = $this->county_model->select();
        $questionaire = $this->questionaire_model->select();
        $quarter = $this->quarter_model->select();
        
        foreach ($county as $value) {
            $this->data['county'][$value->county_id] = $value->name;
        }
        
        foreach ($questionaire as $value) {
            $this->data['questionaire'][$value->questionaire_id] = $value->title;
        }
        
        foreach ($quarter as $value) {
            $this->data['quarter'][$value->quarter_id] = $value->title;
        }

        $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
        $this->template->render();
    }

    function review($response_id = null) {

        $this->data['response_id'] = $response_id;
        $response = $this->county->response($response_id);
        print_r(count($response));
    }

    function update($response_id) {

        $this->data['response_id'] = $response_id;
        $this->data['response'] = $this->response_model->get_response_by_id($response_id);

        if (isset($this->data['response']->id)) {

            if (isset($_POST['submit'])) {

                $post = $_POST;
                unset($post['submit']);

                $this->response_model->set_county_id($post['county_id']);
                $this->response_model->set_quarter_id($post['quarter_id']);
                $this->response_model->set_questionaire_id($post['questionaire_id']);
                $this->response_model->set_response(json_encode($post));
                $this->response_model->set_status(1);

                if ($this->response_model->update() > 0) {
                    $this->message->set('success', 'Success! response saved');
                    redirect(current_url());
                }
            } else {
                $this->data['questionaire'] = $this->questionaire_model->select();
                $this->data['county'] = $this->county_model->select();
                $this->data['quarter'] = $this->quarter_model->select();
                $this->data['question'] = $this->question_model->select();
                $this->template->write_view('content', $this->view_path . __FUNCTION__, $this->data);
                $this->template->render();
            }
        } else {
            
        }
    }

}
