   
        <?php

        if (!defined('BASEPATH'))
            exit('No direct script access allowed');
            
         
        /*
        * @author rotichmordecai
        * -------------------------------------------------------
        * Class name:    Question_model
        * Developed on:  27.05.2017
        * -------------------------------------------------------
        */


        // **********************
        // Class declaration
        // **********************

         
        class Question_model extends CI_Model {
 
        
            private $id;   // DataType: int(11)
            private $title_id;   // DataType: int(11)
            private $question_id;   // DataType: int(11)
            private $question;   // DataType: varchar(255)
            private $description_1;   // DataType: varchar(1500)
            private $description_2;   // DataType: varchar(1500)
            private $description_3;   // DataType: varchar(1500)
            private $question_option;   // DataType: text
            private $score_option;   // DataType: text
            private $created;   // DataType: timestamp
            private $updated;   // DataType: timestamp
            private $status;   // DataType: tinyint(1)
            
        // ********************************************
        // Constructor Method
        // ********************************************

        function __construct()
        {
          parent::__construct();
        }
        
        // ********************************************
        // Getter Methods
        // ********************************************
        
            function get_id()
            {
              return $this->id;
            }
            
            function get_title_id()
            {
              return $this->title_id;
            }
            
            function get_question_id()
            {
              return $this->question_id;
            }
            
            function get_question()
            {
              return $this->question;
            }
            
            function get_description_1()
            {
              return $this->description_1;
            }
            
            function get_description_2()
            {
              return $this->description_2;
            }
            
            function get_description_3()
            {
              return $this->description_3;
            }
            
            function get_question_option()
            {
              return $this->question_option;
            }
            
            function get_score_option()
            {
              return $this->score_option;
            }
            
            function get_created()
            {
              return $this->created;
            }
            
            function get_updated()
            {
              return $this->updated;
            }
            
            function get_status()
            {
              return $this->status;
            }
            
            function get_attribute($attribute) {
            return $this->{ $attribute };
            }
            
        // ********************************************
        // Setter Methods
        // ********************************************
        
            function set_id($value)
            {
              $this->id = $value;
            }
            
            function set_title_id($value)
            {
              $this->title_id = $value;
            }
            
            function set_question_id($value)
            {
              $this->question_id = $value;
            }
            
            function set_question($value)
            {
              $this->question = $value;
            }
            
            function set_description_1($value)
            {
              $this->description_1 = $value;
            }
            
            function set_description_2($value)
            {
              $this->description_2 = $value;
            }
            
            function set_description_3($value)
            {
              $this->description_3 = $value;
            }
            
            function set_question_option($value)
            {
              $this->question_option = $value;
            }
            
            function set_score_option($value)
            {
              $this->score_option = $value;
            }
            
            function set_created($value)
            {
              $this->created = $value;
            }
            
            function set_updated($value)
            {
              $this->updated = $value;
            }
            
            function set_status($value)
            {
              $this->status = $value;
            }
            
            function set_attribute($attribute, $value) {
            $this->{ $attribute } = $value;
            }
            
            
        // ********************************************
        // Init Method
        // ********************************************

        function init($row)
        {        
        
                  $this->id = $row->id;
                  $this->title_id = $row->title_id;
                  $this->question_id = $row->question_id;
                  $this->question = $row->question;
                  $this->description_1 = $row->description_1;
                  $this->description_2 = $row->description_2;
                  $this->description_3 = $row->description_3;
                  $this->question_option = $row->question_option;
                  $this->score_option = $row->score_option;
                  $this->created = $row->created;
                  $this->updated = $row->updated;
                  $this->status = $row->status;
        }
        
        // **********************
        // Select / Get all question
        // **********************

        function select($criteria = null)
        {        
        
                            $this->db->select('question.*');
                            $this->db->select('question.id as question_id');
                            
                            $this->db->select('question.question_id as parent_id');
                            
                            $this->db->select('title.*');
                            $this->db->select('title.id as title_id');
                    
                            
                            $this->db->select('category.*');
                            $this->db->select('category.id as category_id');
                    
                            
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                    
                            
        
        if(is_array($criteria)) {
        $this->db->where($criteria);
        }
        $this->db->from('question');
        $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        $query = $this->db->get();
        return $query->result();

        }
        
            // **********************
            // Get question by id
            // **********************

            function get_question_by_id($id)
            {

            $this->db->select('*');
            $this->db->where('id', $id);
            $query = $this->db->get('question');

            foreach ($query->result() as $question)
            {
                $this->init($question);
                return $question;
            }

            }
        // **********************
        // Insert question
        // **********************

        function insert()
        {
        
        if (isset($this->title_id))$data['title_id'] = $this->title_id;
                      if (isset($this->question_id))$data['question_id'] = $this->question_id;
                      if (isset($this->question))$data['question'] = $this->question;
                      if (isset($this->description_1))$data['description_1'] = $this->description_1;
                      if (isset($this->description_2))$data['description_2'] = $this->description_2;
                      if (isset($this->description_3))$data['description_3'] = $this->description_3;
                      if (isset($this->question_option))$data['question_option'] = $this->question_option;
                      if (isset($this->score_option))$data['score_option'] = $this->score_option;
                      if (isset($this->created))$data['created'] = $this->created;
                      if (isset($this->updated))$data['updated'] = $this->updated;
                      if (isset($this->status))$data['status'] = $this->status;
                      
             $this->db->insert('question', $data); 
             return $this->db->insert_id();
        }
        
        // **********************
        // Delete question
        // **********************

        public function delete($criteria = null)
        {
        
        if($criteria != null) {
        $this->db->delete('question', $criteria); 
            return $this->db->affected_rows();
            }else {
            $this->db->where('id', $this-> id);
            $this->db->delete('question');
                return $this->db->affected_rows();
                }
            
            return 0;
        }
        
        // **********************
        // Count records
        // **********************

        function count()
        {            
            $this->db->select('*');
            $this->db->from('question');
            $query = $this->db->get();
            return $query->num_rows();

        }
            
        // **********************
        // Fetch records
        // **********************

        function fetch($criteria = null, $group_by = null, $order_by = null,$limit = null, $start = null)
        {
        
        
                            $this->db->select('question.*');
                            $this->db->select('question.id as question_id');
                            
                            $this->db->select('question.question_id as parent_id');
                            
                            $this->db->select('title.*');
                            $this->db->select('title.id as title_id');
                    
                            
                            $this->db->select('category.*');
                            $this->db->select('category.id as category_id');
                    
                            
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                    
                            
        
        
            if ($criteria != null && is_array($criteria)) {
                if (count($criteria)) {
                    foreach ($criteria as $key => $value) {
                        if (is_array($value)) {
                           $this->db->where_in($key, $value);
                        } else {
                            $this->db->where($criteria);
                        }
                    }
                } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('question');
        $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count records
        // **********************

        function count_fetch($criteria = null, $group_by = null, $order_by = null)
        {
        
        
                            $this->db->select('question.*');
                            $this->db->select('question.id as question_id');
                            
                            $this->db->select('question.question_id as parent_id');
                            
                            $this->db->select('title.*');
                            $this->db->select('title.id as title_id');
                    
                            
                            $this->db->select('category.*');
                            $this->db->select('category.id as category_id');
                    
                            
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                    
                            
        
        
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('question');
        $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        $query = $this->db->get();
        return $query->num_rows();

        }
            
        // **********************
        // Update question
        // **********************

        function update($criteria = null){
        
        if (isset($this->title_id)) $data['title_id'] = $this->title_id;
                      if (isset($this->question_id)) $data['question_id'] = $this->question_id;
                      if (isset($this->question)) $data['question'] = $this->question;
                      if (isset($this->description_1)) $data['description_1'] = $this->description_1;
                      if (isset($this->description_2)) $data['description_2'] = $this->description_2;
                      if (isset($this->description_3)) $data['description_3'] = $this->description_3;
                      if (isset($this->question_option)) $data['question_option'] = $this->question_option;
                      if (isset($this->score_option)) $data['score_option'] = $this->score_option;
                      if (isset($this->created)) $data['created'] = $this->created;
                      if (isset($this->updated)) $data['updated'] = $this->updated;
                      if (isset($this->status)) $data['status'] = $this->status;
                      
            if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('question', $data); 
            return $this->db->affected_rows();
            } elseif($criteria != null) {
            $this->db->where($criteria); 
            $this->db->update('question', $data); 
            return $this->db->affected_rows();
            } else {
            return 0;
            }}
        
            
        // **********************
        // Search records
        // **********************

        function search($search, $criteria = null, $group_by = null, $order_by = null, $limit = null, $start = null)
        {
        
        $this->db->select('*');$this->db->like('question', $search);
                        $this->db->or_like('description_1', $search);
                        $this->db->or_like('description_2', $search);
                        $this->db->or_like('description_3', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('question');
        $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count search records
        // **********************

        function count_search($search, $criteria = null, $group_by = null, $order_by = null)
        {
            
        $this->db->select('*');$this->db->like('question', $search);
                        $this->db->or_like('description_1', $search);
                        $this->db->or_like('description_2', $search);
                        $this->db->or_like('description_3', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        
        $this->db->from('question');
        $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
        }   
        }       
        
        /* End of file question_model.php */
        /* Location: ./application/models/question_model.php */

        ?>
        