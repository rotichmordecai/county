   
        <?php

        if (!defined('BASEPATH'))
            exit('No direct script access allowed');
            
         
        /*
        * @author rotichmordecai
        * -------------------------------------------------------
        * Class name:    County_model
        * Developed on:  27.05.2017
        * -------------------------------------------------------
        */


        // **********************
        // Class declaration
        // **********************

         
        class County_model extends CI_Model {
 
        
            private $id;   // DataType: int(11)
            private $name;   // DataType: varchar(45)
            private $created;   // DataType: timestamp
            private $updated;   // DataType: timestamp
            private $status;   // DataType: tinyint(1)
            
        // ********************************************
        // Constructor Method
        // ********************************************

        function __construct()
        {
          parent::__construct();
        }
        
        // ********************************************
        // Getter Methods
        // ********************************************
        
            function get_id()
            {
              return $this->id;
            }
            
            function get_name()
            {
              return $this->name;
            }
            
            function get_created()
            {
              return $this->created;
            }
            
            function get_updated()
            {
              return $this->updated;
            }
            
            function get_status()
            {
              return $this->status;
            }
            
            function get_attribute($attribute) {
            return $this->{ $attribute };
            }
            
        // ********************************************
        // Setter Methods
        // ********************************************
        
            function set_id($value)
            {
              $this->id = $value;
            }
            
            function set_name($value)
            {
              $this->name = $value;
            }
            
            function set_created($value)
            {
              $this->created = $value;
            }
            
            function set_updated($value)
            {
              $this->updated = $value;
            }
            
            function set_status($value)
            {
              $this->status = $value;
            }
            
            function set_attribute($attribute, $value) {
            $this->{ $attribute } = $value;
            }
            
            
        // ********************************************
        // Init Method
        // ********************************************

        function init($row)
        {        
        
                  $this->id = $row->id;
                  $this->name = $row->name;
                  $this->created = $row->created;
                  $this->updated = $row->updated;
                  $this->status = $row->status;
        }
        
        // **********************
        // Select / Get all county
        // **********************

        function select($criteria = null)
        {        
        
                            $this->db->select('county.*');
                            $this->db->select('county.id as county_id');
                            
        
        if(is_array($criteria)) {
        $this->db->where($criteria);
        }
        $this->db->from('county');
        
        $query = $this->db->get();
        return $query->result();

        }
        
            // **********************
            // Get county by id
            // **********************

            function get_county_by_id($id)
            {

            $this->db->select('*');
            $this->db->where('id', $id);
            $query = $this->db->get('county');

            foreach ($query->result() as $county)
            {
                $this->init($county);
                return $county;
            }

            }
        // **********************
        // Insert county
        // **********************

        function insert()
        {
        
        if (isset($this->name))$data['name'] = $this->name;
                      if (isset($this->created))$data['created'] = $this->created;
                      if (isset($this->updated))$data['updated'] = $this->updated;
                      if (isset($this->status))$data['status'] = $this->status;
                      
             $this->db->insert('county', $data); 
             return $this->db->insert_id();
        }
        
        // **********************
        // Delete county
        // **********************

        public function delete($criteria = null)
        {
        
        if($criteria != null) {
        $this->db->delete('county', $criteria); 
            return $this->db->affected_rows();
            }else {
            $this->db->where('id', $this-> id);
            $this->db->delete('county');
                return $this->db->affected_rows();
                }
            
            return 0;
        }
        
        // **********************
        // Count records
        // **********************

        function count()
        {            
            $this->db->select('*');
            $this->db->from('county');
            $query = $this->db->get();
            return $query->num_rows();

        }
            
        // **********************
        // Fetch records
        // **********************

        function fetch($criteria = null, $group_by = null, $order_by = null,$limit = null, $start = null)
        {
        
        
                            $this->db->select('county.*');
                            $this->db->select('county.id as county_id');
                            
        
        
            if ($criteria != null && is_array($criteria)) {
                if (count($criteria)) {
                    foreach ($criteria as $key => $value) {
                        if (is_array($value)) {
                           $this->db->where_in($key, $value);
                        } else {
                            $this->db->where($criteria);
                        }
                    }
                } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('county');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count records
        // **********************

        function count_fetch($criteria = null, $group_by = null, $order_by = null)
        {
        
        
                            $this->db->select('county.*');
                            $this->db->select('county.id as county_id');
                            
        
        
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('county');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        $query = $this->db->get();
        return $query->num_rows();

        }
            
        // **********************
        // Update county
        // **********************

        function update($criteria = null){
        
        if (isset($this->name)) $data['name'] = $this->name;
                      if (isset($this->created)) $data['created'] = $this->created;
                      if (isset($this->updated)) $data['updated'] = $this->updated;
                      if (isset($this->status)) $data['status'] = $this->status;
                      
            if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('county', $data); 
            return $this->db->affected_rows();
            } elseif($criteria != null) {
            $this->db->where($criteria); 
            $this->db->update('county', $data); 
            return $this->db->affected_rows();
            } else {
            return 0;
            }}
        
            
        // **********************
        // Search records
        // **********************

        function search($search, $criteria = null, $group_by = null, $order_by = null, $limit = null, $start = null)
        {
        
        $this->db->select('*');$this->db->like('name', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('county');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count search records
        // **********************

        function count_search($search, $criteria = null, $group_by = null, $order_by = null)
        {
            
        $this->db->select('*');$this->db->like('name', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        
        $this->db->from('county');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
        }   
        }       
        
        /* End of file county_model.php */
        /* Location: ./application/models/county_model.php */

        ?>
        