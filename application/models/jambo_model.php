<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of jambo_model
 *
 * @author rotichmordecai
 */
class Jambo_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function database() {
        return $this->db->list_tables();
    }

    public function get_columns($table_name) {
        $query = "SHOW COLUMNS FROM $table_name;";
        return $this->db->query($query)->result();
    }

    function get_foreign_keys($table_name) {
        $database = $this->db->database;
        $query = "SELECT * FROM information_schema.key_column_usage WHERE table_schema = '$database' AND table_name = '$table_name' AND referenced_table_name IS NOT NULL;";
        return $this->db->query($query)->result();
    }

    function get_primary_keys($table_name) {
        $database = $this->db->database;
        $query = "SELECT k.column_name FROM information_schema.table_constraints t JOIN information_schema.key_column_usage k USING(constraint_name,table_schema,table_name) WHERE t.constraint_type='PRIMARY KEY' AND t.table_schema='$database' AND t.table_name='$table_name';";
        return $this->db->query($query)->result();
    }

}
