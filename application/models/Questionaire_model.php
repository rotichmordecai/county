   
        <?php

        if (!defined('BASEPATH'))
            exit('No direct script access allowed');
            
         
        /*
        * @author rotichmordecai
        * -------------------------------------------------------
        * Class name:    Questionaire_model
        * Developed on:  27.05.2017
        * -------------------------------------------------------
        */


        // **********************
        // Class declaration
        // **********************

         
        class Questionaire_model extends CI_Model {
 
        
            private $id;   // DataType: int(11)
            private $title;   // DataType: varchar(45)
            private $created;   // DataType: timestamp
            private $updated;   // DataType: timestamp
            private $status;   // DataType: tinyint(1)
            
        // ********************************************
        // Constructor Method
        // ********************************************

        function __construct()
        {
          parent::__construct();
        }
        
        // ********************************************
        // Getter Methods
        // ********************************************
        
            function get_id()
            {
              return $this->id;
            }
            
            function get_title()
            {
              return $this->title;
            }
            
            function get_created()
            {
              return $this->created;
            }
            
            function get_updated()
            {
              return $this->updated;
            }
            
            function get_status()
            {
              return $this->status;
            }
            
            function get_attribute($attribute) {
            return $this->{ $attribute };
            }
            
        // ********************************************
        // Setter Methods
        // ********************************************
        
            function set_id($value)
            {
              $this->id = $value;
            }
            
            function set_title($value)
            {
              $this->title = $value;
            }
            
            function set_created($value)
            {
              $this->created = $value;
            }
            
            function set_updated($value)
            {
              $this->updated = $value;
            }
            
            function set_status($value)
            {
              $this->status = $value;
            }
            
            function set_attribute($attribute, $value) {
            $this->{ $attribute } = $value;
            }
            
            
        // ********************************************
        // Init Method
        // ********************************************

        function init($row)
        {        
        
                  $this->id = $row->id;
                  $this->title = $row->title;
                  $this->created = $row->created;
                  $this->updated = $row->updated;
                  $this->status = $row->status;
        }
        
        // **********************
        // Select / Get all questionaire
        // **********************

        function select($criteria = null)
        {        
        
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                            
        
        if(is_array($criteria)) {
        $this->db->where($criteria);
        }
        $this->db->from('questionaire');
        
        $query = $this->db->get();
        return $query->result();

        }
        
            // **********************
            // Get questionaire by id
            // **********************

            function get_questionaire_by_id($id)
            {

            $this->db->select('*');
            $this->db->where('id', $id);
            $query = $this->db->get('questionaire');

            foreach ($query->result() as $questionaire)
            {
                $this->init($questionaire);
                return $questionaire;
            }

            }
        // **********************
        // Insert questionaire
        // **********************

        function insert()
        {
        
        if (isset($this->title))$data['title'] = $this->title;
                      if (isset($this->created))$data['created'] = $this->created;
                      if (isset($this->updated))$data['updated'] = $this->updated;
                      if (isset($this->status))$data['status'] = $this->status;
                      
             $this->db->insert('questionaire', $data); 
             return $this->db->insert_id();
        }
        
        // **********************
        // Delete questionaire
        // **********************

        public function delete($criteria = null)
        {
        
        if($criteria != null) {
        $this->db->delete('questionaire', $criteria); 
            return $this->db->affected_rows();
            }else {
            $this->db->where('id', $this-> id);
            $this->db->delete('questionaire');
                return $this->db->affected_rows();
                }
            
            return 0;
        }
        
        // **********************
        // Count records
        // **********************

        function count()
        {            
            $this->db->select('*');
            $this->db->from('questionaire');
            $query = $this->db->get();
            return $query->num_rows();

        }
            
        // **********************
        // Fetch records
        // **********************

        function fetch($criteria = null, $group_by = null, $order_by = null,$limit = null, $start = null)
        {
        
        
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                            
        
        
            if ($criteria != null && is_array($criteria)) {
                if (count($criteria)) {
                    foreach ($criteria as $key => $value) {
                        if (is_array($value)) {
                           $this->db->where_in($key, $value);
                        } else {
                            $this->db->where($criteria);
                        }
                    }
                } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('questionaire');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count records
        // **********************

        function count_fetch($criteria = null, $group_by = null, $order_by = null)
        {
        
        
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                            
        
        
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('questionaire');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        $query = $this->db->get();
        return $query->num_rows();

        }
            
        // **********************
        // Update questionaire
        // **********************

        function update($criteria = null){
        
        if (isset($this->title)) $data['title'] = $this->title;
                      if (isset($this->created)) $data['created'] = $this->created;
                      if (isset($this->updated)) $data['updated'] = $this->updated;
                      if (isset($this->status)) $data['status'] = $this->status;
                      
            if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('questionaire', $data); 
            return $this->db->affected_rows();
            } elseif($criteria != null) {
            $this->db->where($criteria); 
            $this->db->update('questionaire', $data); 
            return $this->db->affected_rows();
            } else {
            return 0;
            }}
        
            
        // **********************
        // Search records
        // **********************

        function search($search, $criteria = null, $group_by = null, $order_by = null, $limit = null, $start = null)
        {
        
        $this->db->select('*');$this->db->like('title', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('questionaire');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count search records
        // **********************

        function count_search($search, $criteria = null, $group_by = null, $order_by = null)
        {
            
        $this->db->select('*');$this->db->like('title', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        
        $this->db->from('questionaire');
        
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
        }   
        }       
        
        /* End of file questionaire_model.php */
        /* Location: ./application/models/questionaire_model.php */

        ?>
        