   
        <?php

        if (!defined('BASEPATH'))
            exit('No direct script access allowed');
            
         
        /*
        * @author rotichmordecai
        * -------------------------------------------------------
        * Class name:    Answer_model
        * Developed on:  27.05.2017
        * -------------------------------------------------------
        */


        // **********************
        // Class declaration
        // **********************

         
        class Answer_model extends CI_Model {
 
        
            private $id;   // DataType: int(11)
            private $response_id;   // DataType: int(11)
            private $question_id;   // DataType: int(11)
            private $value;   // DataType: varchar(45)
            private $created;   // DataType: timestamp
            private $updated;   // DataType: timestamp
            private $status;   // DataType: tinyint(1)
            
        // ********************************************
        // Constructor Method
        // ********************************************

        function __construct()
        {
          parent::__construct();
        }
        
        // ********************************************
        // Getter Methods
        // ********************************************
        
            function get_id()
            {
              return $this->id;
            }
            
            function get_response_id()
            {
              return $this->response_id;
            }
            
            function get_question_id()
            {
              return $this->question_id;
            }
            
            function get_value()
            {
              return $this->value;
            }
            
            function get_created()
            {
              return $this->created;
            }
            
            function get_updated()
            {
              return $this->updated;
            }
            
            function get_status()
            {
              return $this->status;
            }
            
            function get_attribute($attribute) {
            return $this->{ $attribute };
            }
            
        // ********************************************
        // Setter Methods
        // ********************************************
        
            function set_id($value)
            {
              $this->id = $value;
            }
            
            function set_response_id($value)
            {
              $this->response_id = $value;
            }
            
            function set_question_id($value)
            {
              $this->question_id = $value;
            }
            
            function set_value($value)
            {
              $this->value = $value;
            }
            
            function set_created($value)
            {
              $this->created = $value;
            }
            
            function set_updated($value)
            {
              $this->updated = $value;
            }
            
            function set_status($value)
            {
              $this->status = $value;
            }
            
            function set_attribute($attribute, $value) {
            $this->{ $attribute } = $value;
            }
            
            
        // ********************************************
        // Init Method
        // ********************************************

        function init($row)
        {        
        
                  $this->id = $row->id;
                  $this->response_id = $row->response_id;
                  $this->question_id = $row->question_id;
                  $this->value = $row->value;
                  $this->created = $row->created;
                  $this->updated = $row->updated;
                  $this->status = $row->status;
        }
        
        // **********************
        // Select / Get all answer
        // **********************

        function select($criteria = null)
        {        
        
                            $this->db->select('answer.*');
                            $this->db->select('answer.id as answer_id');
                            
                            $this->db->select('question.*');
                            $this->db->select('question.id as question_id');
                    
                            
                            $this->db->select('response.*');
                            $this->db->select('response.id as response_id');
                    
                            
                            $this->db->select('answer.answer_id as parent_id');
                            
                            $this->db->select('title.*');
                            $this->db->select('title.id as title_id');
                    
                            
                            $this->db->select('category.*');
                            $this->db->select('category.id as category_id');
                    
                            
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                    
                            
        
        if(is_array($criteria)) {
        $this->db->where($criteria);
        }
        $this->db->from('answer');
        $this->db->join('question', 'question.id = answer.question_id');
                    $this->db->join('response', 'response.id = answer.response_id');
                    $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        $query = $this->db->get();
        return $query->result();

        }
        
            // **********************
            // Get answer by id
            // **********************

            function get_answer_by_id($id)
            {

            $this->db->select('*');
            $this->db->where('id', $id);
            $query = $this->db->get('answer');

            foreach ($query->result() as $answer)
            {
                $this->init($answer);
                return $answer;
            }

            }
        // **********************
        // Insert answer
        // **********************

        function insert()
        {
        
        if (isset($this->response_id))$data['response_id'] = $this->response_id;
                      if (isset($this->question_id))$data['question_id'] = $this->question_id;
                      if (isset($this->value))$data['value'] = $this->value;
                      if (isset($this->created))$data['created'] = $this->created;
                      if (isset($this->updated))$data['updated'] = $this->updated;
                      if (isset($this->status))$data['status'] = $this->status;
                      
             $this->db->insert('answer', $data); 
             return $this->db->insert_id();
        }
        
        // **********************
        // Delete answer
        // **********************

        public function delete($criteria = null)
        {
        
        if($criteria != null) {
        $this->db->delete('answer', $criteria); 
            return $this->db->affected_rows();
            }else {
            $this->db->where('id', $this-> id);
            $this->db->delete('answer');
                return $this->db->affected_rows();
                }
            
            return 0;
        }
        
        // **********************
        // Count records
        // **********************

        function count()
        {            
            $this->db->select('*');
            $this->db->from('answer');
            $query = $this->db->get();
            return $query->num_rows();

        }
            
        // **********************
        // Fetch records
        // **********************

        function fetch($criteria = null, $group_by = null, $order_by = null,$limit = null, $start = null)
        {
        
        
                            $this->db->select('answer.*');
                            $this->db->select('answer.id as answer_id');
                            
                            $this->db->select('question.*');
                            $this->db->select('question.id as question_id');
                    
                            
                            $this->db->select('response.*');
                            $this->db->select('response.id as response_id');
                    
                            
                            $this->db->select('answer.answer_id as parent_id');
                            
                            $this->db->select('title.*');
                            $this->db->select('title.id as title_id');
                    
                            
                            $this->db->select('category.*');
                            $this->db->select('category.id as category_id');
                    
                            
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                    
                            
        
        
            if ($criteria != null && is_array($criteria)) {
                if (count($criteria)) {
                    foreach ($criteria as $key => $value) {
                        if (is_array($value)) {
                           $this->db->where_in($key, $value);
                        } else {
                            $this->db->where($criteria);
                        }
                    }
                } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('answer');
        $this->db->join('question', 'question.id = answer.question_id');
                    $this->db->join('response', 'response.id = answer.response_id');
                    $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count records
        // **********************

        function count_fetch($criteria = null, $group_by = null, $order_by = null)
        {
        
        
                            $this->db->select('answer.*');
                            $this->db->select('answer.id as answer_id');
                            
                            $this->db->select('question.*');
                            $this->db->select('question.id as question_id');
                    
                            
                            $this->db->select('response.*');
                            $this->db->select('response.id as response_id');
                    
                            
                            $this->db->select('answer.answer_id as parent_id');
                            
                            $this->db->select('title.*');
                            $this->db->select('title.id as title_id');
                    
                            
                            $this->db->select('category.*');
                            $this->db->select('category.id as category_id');
                    
                            
                            $this->db->select('questionaire.*');
                            $this->db->select('questionaire.id as questionaire_id');
                    
                            
        
        
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('answer');
        $this->db->join('question', 'question.id = answer.question_id');
                    $this->db->join('response', 'response.id = answer.response_id');
                    $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        $query = $this->db->get();
        return $query->num_rows();

        }
            
        // **********************
        // Update answer
        // **********************

        function update($criteria = null){
        
        if (isset($this->response_id)) $data['response_id'] = $this->response_id;
                      if (isset($this->question_id)) $data['question_id'] = $this->question_id;
                      if (isset($this->value)) $data['value'] = $this->value;
                      if (isset($this->created)) $data['created'] = $this->created;
                      if (isset($this->updated)) $data['updated'] = $this->updated;
                      if (isset($this->status)) $data['status'] = $this->status;
                      
            if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('answer', $data); 
            return $this->db->affected_rows();
            } elseif($criteria != null) {
            $this->db->where($criteria); 
            $this->db->update('answer', $data); 
            return $this->db->affected_rows();
            } else {
            return 0;
            }}
        
            
        // **********************
        // Search records
        // **********************

        function search($search, $criteria = null, $group_by = null, $order_by = null, $limit = null, $start = null)
        {
        
        $this->db->select('*');$this->db->like('value', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('answer');
        $this->db->join('question', 'question.id = answer.question_id');
                    $this->db->join('response', 'response.id = answer.response_id');
                    $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        
        if( isset($limit) && isset($start) ) {
            $this->db->limit($limit, $start);
        }
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

        }
        // **********************
        // Count search records
        // **********************

        function count_search($search, $criteria = null, $group_by = null, $order_by = null)
        {
            
        $this->db->select('*');$this->db->like('value', $search);
                        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                       $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        
        $this->db->from('answer');
        $this->db->join('question', 'question.id = answer.question_id');
                    $this->db->join('response', 'response.id = answer.response_id');
                    $this->db->join('title', 'title.id = question.title_id');
                    $this->db->join('category', 'category.id = title.category_id');
                    $this->db->join('questionaire', 'questionaire.id = category.questionaire_id');
                    
        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }
        
        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
        }   
        }       
        
        /* End of file answer_model.php */
        /* Location: ./application/models/answer_model.php */

        ?>
        