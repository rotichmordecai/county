   
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
 * @author rotichmordecai
 * -------------------------------------------------------
 * Class name:    Response_model
 * Developed on:  27.05.2017
 * -------------------------------------------------------
 */

// **********************
// Class declaration
// **********************


class Response_model extends CI_Model {

    private $id;   // DataType: int(11)
    private $county_id;   // DataType: int(11)
    private $quarter_id;   // DataType: int(11)
    private $questionaire_id;   // DataType: int(11)
    private $response;   // DataType: text
    private $created;   // DataType: timestamp
    private $updated;   // DataType: timestamp
    private $status;   // DataType: tinyint(1)

    // ********************************************
    // Constructor Method
    // ********************************************

    function __construct() {
        parent::__construct();
    }

    // ********************************************
    // Getter Methods
    // ********************************************

    function get_id() {
        return $this->id;
    }

    function get_county_id() {
        return $this->county_id;
    }

    function get_quarter_id() {
        return $this->quarter_id;
    }

    function get_questionaire_id() {
        return $this->questionaire_id;
    }

    function get_response() {
        return $this->response;
    }

    function get_created() {
        return $this->created;
    }

    function get_updated() {
        return $this->updated;
    }

    function get_status() {
        return $this->status;
    }

    function get_attribute($attribute) {
        return $this->{ $attribute };
    }

    // ********************************************
    // Setter Methods
    // ********************************************

    function set_id($value) {
        $this->id = $value;
    }

    function set_county_id($value) {
        $this->county_id = $value;
    }

    function set_quarter_id($value) {
        $this->quarter_id = $value;
    }

    function set_questionaire_id($value) {
        $this->questionaire_id = $value;
    }

    function set_response($value) {
        $this->response = $value;
    }

    function set_created($value) {
        $this->created = $value;
    }

    function set_updated($value) {
        $this->updated = $value;
    }

    function set_status($value) {
        $this->status = $value;
    }

    function set_attribute($attribute, $value) {
        $this->{ $attribute } = $value;
    }

    // ********************************************
    // Init Method
    // ********************************************

    function init($row) {

        $this->id = $row->id;
        $this->county_id = $row->county_id;
        $this->quarter_id = $row->quarter_id;
        $this->questionaire_id = $row->questionaire_id;
        $this->response = $row->response;
        $this->created = $row->created;
        $this->updated = $row->updated;
        $this->status = $row->status;
    }

    // **********************
    // Select / Get all response
    // **********************

    function select($criteria = null) {

        $this->db->select('response.*');
        $this->db->select('response.id as response_id');

        $this->db->select('county.*');
        $this->db->select('county.id as county_id');


        $this->db->select('quarter.*');
        $this->db->select('quarter.id as quarter_id');


        $this->db->select('questionaire.*');
        $this->db->select('questionaire.id as questionaire_id');



        if (is_array($criteria)) {
            $this->db->where($criteria);
        }
        
        $this->db->from('response');
        $this->db->join('county', 'county.id = response.county_id');
        $this->db->join('quarter', 'quarter.id = response.quarter_id');
        $this->db->join('questionaire', 'questionaire.id = response.questionaire_id');

        $query = $this->db->get();
        return $query->result();
    }

    // **********************
    // Get response by id
    // **********************

    function get_response_by_id($id) {

        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('response');

        foreach ($query->result() as $response) {
            $this->init($response);
            return $response;
        }
       
    }

    // **********************
    // Insert response
    // **********************

    function insert() {

        if (isset($this->county_id))
            $data['county_id'] = $this->county_id;
        if (isset($this->quarter_id))
            $data['quarter_id'] = $this->quarter_id;
        if (isset($this->questionaire_id))
            $data['questionaire_id'] = $this->questionaire_id;
        if (isset($this->response))
            $data['response'] = $this->response;
        if (isset($this->created))
            $data['created'] = $this->created;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        $this->db->insert('response', $data);
        return $this->db->insert_id();
    }

    // **********************
    // Delete response
    // **********************

    public function delete($criteria = null) {

        if ($criteria != null) {
            $this->db->delete('response', $criteria);
            return $this->db->affected_rows();
        } else {
            $this->db->where('id', $this->id);
            $this->db->delete('response');
            return $this->db->affected_rows();
        }

        return 0;
    }

    // **********************
    // Count records
    // **********************

    function count() {
        $this->db->select('*');
        $this->db->from('response');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Fetch records
    // **********************

    function fetch($criteria = null, $group_by = null, $order_by = null, $limit = null, $start = null) {


        $this->db->select('response.*');
        $this->db->select('response.id as response_id');

        $this->db->select('county.*');
        $this->db->select('county.id as county_id');


        $this->db->select('quarter.*');
        $this->db->select('quarter.id as quarter_id');


        $this->db->select('questionaire.*');
        $this->db->select('questionaire.id as questionaire_id');




        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('response');
        $this->db->join('county', 'county.id = response.county_id');
        $this->db->join('quarter', 'quarter.id = response.quarter_id');
        $this->db->join('questionaire', 'questionaire.id = response.questionaire_id');

        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }

        if (isset($limit) && isset($start)) {
            $this->db->limit($limit, $start);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count records
    // **********************

    function count_fetch($criteria = null, $group_by = null, $order_by = null) {


        $this->db->select('response.*');
        $this->db->select('response.id as response_id');

        $this->db->select('county.*');
        $this->db->select('county.id as county_id');


        $this->db->select('quarter.*');
        $this->db->select('quarter.id as quarter_id');


        $this->db->select('questionaire.*');
        $this->db->select('questionaire.id as questionaire_id');




        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('response');
        $this->db->join('county', 'county.id = response.county_id');
        $this->db->join('quarter', 'quarter.id = response.quarter_id');
        $this->db->join('questionaire', 'questionaire.id = response.questionaire_id');

        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // **********************
    // Update response
    // **********************

    function update($criteria = null) {

        if (isset($this->county_id))
            $data['county_id'] = $this->county_id;
        if (isset($this->quarter_id))
            $data['quarter_id'] = $this->quarter_id;
        if (isset($this->questionaire_id))
            $data['questionaire_id'] = $this->questionaire_id;
        if (isset($this->response))
            $data['response'] = $this->response;
        if (isset($this->created))
            $data['created'] = $this->created;
        if (isset($this->updated))
            $data['updated'] = $this->updated;
        if (isset($this->status))
            $data['status'] = $this->status;

        if ($this->id > 0) {
            $this->db->where('id', $this->id);
            $this->db->update('response', $data);
            return $this->db->affected_rows();
        } elseif ($criteria != null) {
            $this->db->where($criteria);
            $this->db->update('response', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    // **********************
    // Search records
    // **********************

    function search($search, $criteria = null, $group_by = null, $order_by = null, $limit = null, $start = null) {

        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }
        $this->db->from('response');
        $this->db->join('county', 'county.id = response.county_id');
        $this->db->join('quarter', 'quarter.id = response.quarter_id');
        $this->db->join('questionaire', 'questionaire.id = response.questionaire_id');

        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }

        if (isset($limit) && isset($start)) {
            $this->db->limit($limit, $start);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    // **********************
    // Count search records
    // **********************

    function count_search($search, $criteria = null, $group_by = null, $order_by = null) {

        $this->db->select('*');
        if ($criteria != null && is_array($criteria)) {
            if (count($criteria)) {
                foreach ($criteria as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } else {
                        $this->db->where($criteria);
                    }
                }
            } else {
                $this->db->where($criteria);
            }
        }

        $this->db->from('response');
        $this->db->join('county', 'county.id = response.county_id');
        $this->db->join('quarter', 'quarter.id = response.quarter_id');
        $this->db->join('questionaire', 'questionaire.id = response.questionaire_id');

        if ($group_by != NULL) {
            $this->hm->group_by($group_by);
        }

        if ($order_by != NULL) {
            $this->hm->order_by($order_by);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

}

/* End of file response_model.php */
/* Location: ./application/models/response_model.php */
?>
        