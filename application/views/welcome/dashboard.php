<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--Start Breadcrumb-->
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="#">Dashboard</a></li>
        </ol>
    </div>
</div>

<!--End Breadcrumb-->
<!--Start Dashboard 1-->
<div id="dashboard-header" class="row">
    <div class="col-xs-10 col-sm-2">
        <h3>Welcome!</h3>
    </div>
    <div class="col-xs-2 col-sm-1 col-sm-offset-1">
    </div>
    <div class="clearfix visible-xs"></div>
    <div class="col-xs-12 col-sm-8 col-md-7 pull-right">
        <div class="row">

            <div class="col-xs-3">
                <a class="btn btn-success" href="<?php echo site_url('welcome/index'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <span class="hidden-xs">New</span>
                </a>
            </div>            
            <div class="col-xs-3">
                <div class="sparkline-dashboard" id="sparkline-1"></div>
                <div class="sparkline-dashboard-info">
                    <i class="fa fa-map-marker"></i> <?php echo count($count['county_id']); ?>
                    <span class="txt-info">Counties</span>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="sparkline-dashboard" id="sparkline-2"></div>
                <div class="sparkline-dashboard-info">
                    <i class="fa fa-book"></i> <?php echo count($count['questionaire_id']); ?>
                    <span class="txt-info">Questionnaires</span>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="sparkline-dashboard" id="sparkline-3"></div>
                <div class="sparkline-dashboard-info">
                    <i class="fa fa-clock-o"></i> <?php echo count($count['quarter_id']); ?>
                    <span class="txt-info">Quarters</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Dashboard 1-->
<!--Start Dashboard 2-->
<div class="row-fluid">
    <div id="dashboard_links" class="col-xs-12 col-sm-2 pull-right">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#" class="tab-link" id="overview">Overview</a></li>
            <li><a href="#" class="tab-link" id="clients">Recent</a></li>
            <li><a href="#" class="tab-link" id="progress">Progress</a></li>
        </ul>
    </div>
    <div id="dashboard_tabs" class="col-xs-12 col-sm-10">
        <!--Start Dashboard Tab 1-->
        <div id="dashboard-overview" class="row" style="visibility: visible; position: relative;">
            <div id="ow-marketplace" class="col-sm-12 col-md-6">
                <h4 class="page-header">Summary</h4>
                <table id="ticker-table" class="table m-table table-bordered table-hover table-heading">
                    <thead>
                        <tr>
                            <th>County</th>
                            <th>Questionnaire</th>
                            <th>MPC STATUS</th>
                            <th>TOTAL SCORE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($response as $_response): ?>
                            <tr>
                                <td class="m-ticker"><b><?php echo $county[$_response['detail']['county_id']]; ?></b><span><?php echo $quarter[$_response['detail']['county_id']]; ?></span></td>
                                <td class="m-price"><?php echo $questionaire[$_response['detail']['questionaire_id']]; ?></td>
                                <td class="m-change">                                    
                                    <?php foreach ($_response['perform'] as $key => $value): ?>
                                        <?php
                                        $class = NULL;
                                        switch ($value) {
                                            case 'Approved':
                                                $class = 'label label-primary';
                                                break;
                                            case 'Complete':
                                                $class = 'label label-success';
                                                break;
                                            case 'Nearly Complete':
                                                $class = 'label label-almost';
                                                break;
                                            case 'Issue':
                                                $class = 'label label-warning';
                                                break;
                                            case 'Unknown':
                                                $class = 'label label-danger';
                                                break;
                                            default:
                                                break;
                                        }
                                        ?>
                                        <span class="<?php echo $class; ?>"><?php echo $key; ?></span>
                                    <?php endforeach; ?>                                    
                                </td>
                                <td class="td-graph">
                                    <?php
                                    $sum = 0;
                                    foreach ($_response['measure'] as $measure) {
                                        $sum += $measure;
                                    }
                                    echo $sum;
                                    ?>                                    
                                </td>
                            </tr>
                        <?php endforeach; ?>                        
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12 col-md-6">
                <div id="ow-donut" class="row">
                    <div class="col-xs-4">
                        <div id="morris_donut_1" style="width:120px;height:60px;"></div>
                    </div>
                    <div class="col-xs-4">
                        <div id="morris_donut_2" style="width:120px;height:60px;"></div>
                    </div>
                    <div class="col-xs-4">
                        <div id="morris_donut_3" style="width:120px;height:60px;"></div>
                    </div>
                </div>
                <div id="ow-activity" class="row">
                    <div class="col-xs-2 col-sm-1 col-md-2">
                    </div>
                    <div class="col-xs-7 col-sm-5 col-md-6">
                        <div class="row"><i class="fa fa fa-money"></i> Enhanced revenue management <span class="label label-default pull-right">Details</span></div>
                        <div class="row"><i class="fa fa-briefcase"></i> Internal audit <span class="label label-default pull-right">Details</span></div>
                    </div>
                    <div id="ow-stat" class="col-xs-3 col-sm-4 col-md-4 pull-right">
                    </div>
                </div>
                <br /><br />
                <div id="ow-summary" class="row">
                    <div class="col-xs-12">
                        <h4 class="page-header">SUMMARY</h4>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-6">Total questionnaires<b>1245634</b></div>
                                    <div class="col-xs-6">Release count<b>227</b></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">Completed questionnaires<b>5222345</b></div>
                                    <div class="col-xs-6">Highest score<b>324322</b></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">Incomplete questionnaires<b>52145</b></div>
                                    <div class="col-xs-6">Lowest score<b>288</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Dashboard Tab 1-->
        <!--Start Dashboard Tab 2-->
        <div id="dashboard-clients" class="row" style="visibility: hidden; position: absolute;">
            <div class="row one-list-message">
                <div class="col-xs-1"><i class="fa fa-users"></i></div>
                <div class="col-xs-2"><b></b></div>
                <div class="col-xs-2"></div>
                <div class="col-xs-2"></div>
                <div class="col-xs-2">Update</div>
            </div>
            <?php foreach ($recent as $_recent): ?>
                <div class="row one-list-message">
                    <div class="col-xs-1"><i class="fa fa-book"></i></div>
                    <div class="col-xs-2">
                        <?php echo $_recent->response_id; ?>
                    </div>
                    <div class="col-xs-2">
                        <?php echo $_recent->name; ?>
                    </div>
                    <div class="col-xs-2">
                        <?php echo $_recent->title; ?>
                    </div>
                    <div class="col-xs-2"><i class="fa fa-link"></i> <?php echo anchor('welcome/update/'.$_recent->response_id, 'Update', 'title="Update response"'); ?> </div>
                </div>
            <?php endforeach; ?>  
            <img src="<?php echo base_url();?>/assets/report.png" alt="" style="">
        </div>
        <!--End Dashboard Tab 2-->   
        <div id="dashboard-progress" class="row" style="visibility: hidden; position: absolute;">
            <br />
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="box ui-draggable ui-droppable">
                        <div class="box-header">
                            <div class="box-name">
                                <i class="fa fa-table"></i>
                                <span>Revenue Enhancement</span>
                            </div>
                            <div class="box-icons">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="expand-link">
                                    <i class="fa fa-expand"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="no-move"></div>
                        </div>
                        <div class="box-content">
                            <p>Progress in Revenue Enhancement 2015 - 2106</p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                    <span>30% Nairobi</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                                    <span>40% Nakuru</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    <span>60% Mombasa</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                    <span class="sr-only">70% Kiambu</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                                    <span>50% Tana River</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--End Dashboard 2 -->
<div style="height: 40px;"></div>
<script type="text/javascript">
// Array for random data for Sparkline
    var sparkline_arr_1 = SparklineTestData();
    var sparkline_arr_2 = SparklineTestData();
    var sparkline_arr_3 = SparklineTestData();
    $(document).ready(function () {
        // Make all JS-activity for dashboard
        DashboardTabChecker();
        // Load Knob plugin and run callback for draw Knob charts for dashboard(tab-servers)
        LoadKnobScripts(DrawKnobDashboard);
        // Load Sparkline plugin and run callback for draw Sparkline charts for dashboard(top of dashboard + plot in tables)
        LoadSparkLineScript(DrawSparklineDashboard);
        // Load Morris plugin and run callback for draw Morris charts for dashboard
        LoadMorrisScripts(MorrisDashboard);
        // Make beauty hover in table
        $("#ticker-table").beautyHover();
    });
</script>
