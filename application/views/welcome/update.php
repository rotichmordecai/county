<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$response->response = json_decode($response->response);

?>


<div class="panel panel-success" id="panel">
    <div class="panel-heading collapseable" data-toggle="collapse" data-target="##QuestionForm">
        <h3 class="panel-title">Self Assessment Questionnaire</h3>                        
    </div>
    <input type="hidden" name="Sme_Product_Number_1" value="" id="Sme_Product_Number_1">
    <div id="QuestionForm" class="panel-collapse collapse in">
        <div class="panel-body">

            <?php
            $attributes = array('class' => 'form-horizontal', 'id' => 'questionnaire');
            echo form_open(current_url(), $attributes);
            ?>

            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">County</label>
                <div class="col-md-4">      
                    <select class="form-control required" name="county_id" >                    
                        <?php foreach ($county as $county_value): ?>
                            <option value="<?php echo $county_value->id; ?>" <?php if ($response->county_id == $county_value->id): ?>selected="selected"<?php endif; ?>><?php echo $county_value->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Questionaire</label>
                <div class="col-md-4">      
                    <select class="form-control required" name="questionaire_id" >                    
                        <?php foreach ($questionaire as $questionaire_value): ?>
                            <option value="<?php echo $questionaire_value->id; ?>" <?php if ($response->questionaire_id == $questionaire_value->id): ?>selected="selected"<?php endif; ?>><?php echo $questionaire_value->title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Quarter</label>
                <div class="col-md-4">      
                    <select class="form-control required" name="quarter_id" >                    
                        <?php foreach ($quarter as $quarter_value): ?>
                            <option value="<?php echo $quarter_value->id; ?>" <?php if ($response->quarter_id == $quarter_value->id): ?>selected="selected"<?php endif; ?>><?php echo $quarter_value->title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <table class="table table-bordered">
                <tr>                    
                    <th></th>
                    <th></th>
                    <th></th>
                    <th width="450px"></th>
                </tr>

                <?php foreach ($question as $value): ?>

                    <tr title="Question 1" data-toggle="popover" data-content="This is a declaration of whether or not conflict metals are intentionally added to your product by your company or your supply chain. This question shall be answered for each conflict metal. Valid responses to this question are either 'Yes' or 'No'. This question is mandatory.<br><b>Note:</b> Some companies may require substantiation for a 'No' answer that should be entered into the Comment Field.">
                        <?php if (strlen($value->description_2)): ?>
                            <td>
                                <?php echo $value->question; ?>                            
                            </td>
                            <td>
                                <?php echo $value->description_1; ?>                                
                                <?php if (is_array(json_decode($value->question_option, TRUE))): ?>
                                    <br />
                                    <br />
                                    <?php
                                    // print_r(json_decode($value->question_option, TRUE));
                                    ?>
                                    <ul class="list-unstyled">                    
                                        <?php foreach (json_decode($value->question_option, TRUE) as $option_key => $option_value): ?>
                                            <li><?php echo $option_key; ?>) <?php echo $option_value['text']; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>

                            </td>
                            <td>
                                <?php echo $value->description_2; ?>
                            </td>
                            <td>
                                <?php if (is_array(json_decode($value->score_option, TRUE))): ?>
                                    <div class="form-group">                                    
                                        <?php $answer_options = json_decode($value->score_option, TRUE); ?>
                                        <?php if (isset($answer_options['select'])): ?>
                                            <div class="col-md-12">
                                                <select class="form-control required" name="<?php echo $value->question_id; ?>" >                    
                                                    <?php foreach ($answer_options['select'] as $select_value): ?>
                                                        <option value="<?php echo $select_value; ?>" <?php if ($response->response->{$value->question_id} == $select_value): ?>selected="selected"<?php endif; ?>><?php echo $select_value; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        <?php endif; ?>                                   
                                    </div>
                                <?php else: ?>

                                    <?php if (is_array(json_decode($value->question_option, TRUE))): ?>

                                        <table>
                                            <?php foreach (json_decode($value->question_option, TRUE) as $option_key => $option_value): ?>
                                                <tr>
                                                    <td><?php echo $option_key; ?>)</td>
                                                    <td>
                                                        <?php if (isset($option_value['form']['checkbox'])): ?>
                                                            <div class="toggle-switch toggle-switch-success">
                                                                <div class="col-sm-2">
                                                                    <div class="toggle-switch">
                                                                        <label>         
                                                                            <?php if (isset($response->response->{$value->question_id})): ?>
                                                                                <input type="checkbox" <?php if (isset($response->response->{$value->question_id}->{$option_key})): ?>checked="checked"<?php endif; ?> name="<?php echo $value->question_id; ?>[<?php echo $option_key; ?>]" value="<?php echo $option_value['form']['checkbox']; ?>">                                                                        
                                                                            <?php else: ?>
                                                                                <input type="checkbox" name="<?php echo $value->question_id; ?>[<?php echo $option_key; ?>]" value="<?php echo $option_value['form']['checkbox']; ?>">                                                                        
                                                                            <?php endif; ?>
                                                                            <div class="toggle-switch-inner"></div>
                                                                            <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php elseif (isset($option_value['form']['select'])): ?>
                                                            <div class="col-md-12">
                                                                <select class="form-control required" name="<?php echo $value->question_id; ?>[<?php echo $option_key; ?>]" >                    
                                                                    <?php foreach ($option_value['form']['select'] as $select_value): ?>
                                                                        <option value="<?php echo $select_value; ?>" <?php if (isset($response->response->{$value->question_id}->{$option_key}) && $response->response->{$value->question_id}->{$option_key} == $select_value): ?>selected="selected"<?php endif; ?>><?php echo $select_value; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>

                                                        <?php endif; ?>
                                                    </td>
                                                </tr>

                                            <?php endforeach; ?>
                                        </table>

                                    <?php else: ?>
                                        <div class="toggle-switch toggle-switch-success">
                                            <div class="col-sm-2">
                                                <div class="toggle-switch">
                                                    <label>

                                                        <?php if (isset($response->response->{$value->question_id})): ?>
                                                            <input type="checkbox" <?php if ($response->response->{$value->question_id} == '1'): ?>checked="checked"<?php endif; ?> name="<?php echo $value->question_id; ?>" value="1">                                                                        
                                                        <?php else: ?>
                                                            <input type="checkbox" name="<?php echo $value->question_id; ?>" value="1">                                                                        
                                                        <?php endif; ?>

                                                        <div class="toggle-switch-inner"></div>
                                                        <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                <?php endif; ?>
                            </td>
                        <?php else: ?>
                            <td colspan="4">
                                <strong>
                                    <?php echo $value->question; ?> 
                                </strong>                                                       
                            </td>
                        <?php endif; ?>

                    </tr>
                <?php endforeach; ?>
            </table>

            <?php
            echo form_submit('submit', 'Submit!');
            echo form_reset('reset', 'Reset!');
            echo form_close();
            ?>
        </div>
    </div>









