<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<html ng-app="my_app" lang="en">
    <head>
        <meta charset="utf-8">
        <title>KADSP - County Performance Assessments</title>
        <meta name="description" content="description">
        <meta name="author" content="DevOOPS">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php echo $_styles; ?>
        <?php echo $_scripts; ?>

        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                        <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
                        <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            //var csrf_value = '<?php print $this->security->get_csrf_hash(); ?>';
        </script>
        <script type="text/javascript">
            var ng_app = angular.module('my_app', []);
        </script>

    </head>
    <body>
        <!--Start Header-->
        <div id="screensaver">
            <canvas id="canvas"></canvas>
            <i class="fa fa-lock" id="screen_unlock"></i>
        </div>
        <div id="modalbox">
            <div class="devoops-modal">
                <div class="devoops-modal-header">
                    <div class="modal-header-name">
                        <span>Basic table</span>
                    </div>
                    <div class="box-icons">
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="devoops-modal-inner">
                </div>
                <div class="devoops-modal-bottom">
                </div>
            </div>
        </div>
        <header class="navbar">
            <div class="container-fluid expanded-panel">
                <div class="row">
                    <div id="logo" class="col-xs-12 col-sm-2">
                        <a href="">KADSP</a>
                    </div>
                    <div id="top-panel" class="col-xs-12 col-sm-10">
                        <div class="row">
                            <div class="col-xs-8 col-sm-4">
                                <a href="#" class="show-sidebar">
                                    <i class="fa fa-bars"></i>
                                </a></div>
                            <div class="col-xs-4 col-sm-8 top-panel-right">
                                <ul class="nav navbar-nav pull-right panel-menu">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle account" data-toggle="dropdown"><i class="fa fa-angle-down pull-right"></i>
                                            <div class="user-mini pull-right">
                                                <span class="welcome">Welcome,</span>Admin</div>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user"></i>
                                                    <span>Profile</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="ajax/page_messages.html" class="ajax-link">
                                                    <i class="fa fa-envelope"></i>
                                                    <span>Messages</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="ajax/gallery_simple.html" class="ajax-link">
                                                    <i class="fa fa-picture-o"></i>
                                                    <span>Albums</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="ajax/calendar.html" class="ajax-link">
                                                    <i class="fa fa-tasks"></i>
                                                    <span>Tasks</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-cog"></i>
                                                    <span>Settings</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-power-off"></i>
                                                    <span>Logout</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--End Header-->
        <!--Start Container-->
        <div id="main" class="container-fluid">
            <div class="row">
                <div id="sidebar-left" class="col-xs-2 col-sm-2">
                    <ul class="nav main-menu">
                        <li>
                            <a class="active" href="<?php echo site_url('welcome/dashboard'); ?>">
                                <i class="fa fa-dashboard"></i>
                                <span class="hidden-xs">Dashboard</span>
                            </a>
                        </li>                       
                    </ul>
                </div>
                <!--Start Content-->
                <div id="content" class="col-xs-12 col-sm-10">
                    <?php echo isset($content) ? $content : ''; ?>                     
                    <div class="preloader">
                        <img src="img/devoops_getdata.gif" hidden="hidden" class="devoops-getdata" alt="preloader"/>
                    </div>
                    <div id="ajax-content">

                    </div>

                </div>
                <!--End Content-->
            </div>
        </div>

        <!--End Container-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--<script src="http://code.jquery.com/jquery.js"></script>-->

    </body>
</html>