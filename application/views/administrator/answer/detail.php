
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="AnswerController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Answer</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        
<div class="bs-example" data-example-id="simple-table"> <table class="table"> <caption>Details - Answer</caption> <tbody> 
                         
                                    <tr> <th scope="row">id</th> <td> <?php echo $answer->id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">response_id</th> <td> <?php echo $answer->response_id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">question_id</th> <td> <?php echo $answer->question_id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">value</th> <td> <?php echo $answer->value; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">status</th> <td> <?php echo $answer->status; ?> </td> </tr>                                    
                              
            </tbody> </table> </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  