<script type="text/javascript">
                    ng_app.controller('AnswerController', function ($scope) {
        $scope.answer = {
        
            id: '<?php echo set_value('id',isset($answer->id)?$answer->id:''); ?>',
            response_id: '<?php echo set_value('response_id',isset($answer->response_id)?$answer->response_id:''); ?>',
            question_id: '<?php echo set_value('question_id',isset($answer->question_id)?$answer->question_id:''); ?>',
            value: '<?php echo set_value('value',isset($answer->value)?$answer->value:''); ?>',
            created: '<?php echo set_value('created',isset($answer->created)?$answer->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($answer->updated)?$answer->updated:''); ?>',
            status: '<?php echo set_value('status',isset($answer->status)?$answer->status:''); ?>',
            response: '<?php echo json_encode($response); ?>',
            question: '<?php echo json_encode($question); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="AnswerController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Answer</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'answer-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('response_id')) > 0 ? 'has-error' : '' ?>">
                                <label>response_id</label>
                                <select id="response_id" name="response_id" ng-model="answer.response_id" ng-value="answer.response_id">
                                    <?php foreach ($select_response as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('response_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('question_id')) > 0 ? 'has-error' : '' ?>">
                                <label>question_id</label>
                                <select id="question_id" name="question_id" ng-model="answer.question_id" ng-value="answer.question_id">
                                    <?php foreach ($select_question as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('question_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('value')) > 0 ? 'has-error' : '' ?>">
                                <label>value</label>
                                <input class="form-control" id="value" name="value" ng-model="answer.value" ng-value="answer.value" value="<?php echo set_value('value',isset($answer->value)?$answer->value:''); ?>"  placeholder="Value">
                                <p class="help-block"><?php echo form_error('value'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="answer.status" ng-value="answer.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  