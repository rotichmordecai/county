
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="QuestionController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Question</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        
<div class="bs-example" data-example-id="simple-table"> <table class="table"> <caption>Details - Question</caption> <tbody> 
                         
                                    <tr> <th scope="row">id</th> <td> <?php echo $question->id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">title_id</th> <td> <?php echo $question->title_id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">question_id</th> <td> <?php echo $question->question_id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">question</th> <td> <?php echo $question->question; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">description_1</th> <td> <?php echo $question->description_1; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">description_2</th> <td> <?php echo $question->description_2; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">description_3</th> <td> <?php echo $question->description_3; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">question_option</th> <td> <?php echo $question->question_option; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">score_option</th> <td> <?php echo $question->score_option; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">status</th> <td> <?php echo $question->status; ?> </td> </tr>                                    
                              
            </tbody> </table> </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  