<script type="text/javascript">
                    ng_app.controller('QuestionController', function ($scope) {
        $scope.question = {
        
            id: '<?php echo set_value('id',isset($question->id)?$question->id:''); ?>',
            title_id: '<?php echo set_value('title_id',isset($question->title_id)?$question->title_id:''); ?>',
            question_id: '<?php echo set_value('question_id',isset($question->question_id)?$question->question_id:''); ?>',
            question: '<?php echo set_value('question',isset($question->question)?$question->question:''); ?>',
            description_1: '<?php echo set_value('description_1',isset($question->description_1)?$question->description_1:''); ?>',
            description_2: '<?php echo set_value('description_2',isset($question->description_2)?$question->description_2:''); ?>',
            description_3: '<?php echo set_value('description_3',isset($question->description_3)?$question->description_3:''); ?>',
            question_option: '<?php echo set_value('question_option',isset($question->question_option)?$question->question_option:''); ?>',
            score_option: '<?php echo set_value('score_option',isset($question->score_option)?$question->score_option:''); ?>',
            created: '<?php echo set_value('created',isset($question->created)?$question->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($question->updated)?$question->updated:''); ?>',
            status: '<?php echo set_value('status',isset($question->status)?$question->status:''); ?>',
            title: '<?php echo json_encode($title); ?>',
            question: '<?php echo json_encode($question); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="QuestionController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Question</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'question-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('title_id')) > 0 ? 'has-error' : '' ?>">
                                <label>title_id</label>
                                <select id="title_id" name="title_id" ng-model="question.title_id" ng-value="question.title_id">
                                    <?php foreach ($select_title as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('title_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('question_id')) > 0 ? 'has-error' : '' ?>">
                                <label>question_id</label>
                                <select id="question_id" name="question_id" ng-model="question.question_id" ng-value="question.question_id">
                                    <?php foreach ($select_question as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('question_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('question')) > 0 ? 'has-error' : '' ?>">
                                <label>question</label>
                                <textarea class="form-control" id="question" name="question" ng-model="question.question" ng-value="question.question"><?php echo set_value('question',isset($question->question)?$question->question:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('question'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('description_1')) > 0 ? 'has-error' : '' ?>">
                                <label>description_1</label>
                                <textarea class="form-control" id="description_1" name="description_1" ng-model="question.description_1" ng-value="question.description_1"><?php echo set_value('description_1',isset($question->description_1)?$question->description_1:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('description_1'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('description_2')) > 0 ? 'has-error' : '' ?>">
                                <label>description_2</label>
                                <textarea class="form-control" id="description_2" name="description_2" ng-model="question.description_2" ng-value="question.description_2"><?php echo set_value('description_2',isset($question->description_2)?$question->description_2:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('description_2'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('description_3')) > 0 ? 'has-error' : '' ?>">
                                <label>description_3</label>
                                <textarea class="form-control" id="description_3" name="description_3" ng-model="question.description_3" ng-value="question.description_3"><?php echo set_value('description_3',isset($question->description_3)?$question->description_3:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('description_3'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('question_option')) > 0 ? 'has-error' : '' ?>">
                                <label>question_option</label>
                                <textarea class="form-control" id="question_option" name="question_option" ng-model="question.question_option" ng-value="question.question_option"><?php echo set_value('question_option',isset($question->question_option)?$question->question_option:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('question_option'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('score_option')) > 0 ? 'has-error' : '' ?>">
                                <label>score_option</label>
                                <textarea class="form-control" id="score_option" name="score_option" ng-model="question.score_option" ng-value="question.score_option"><?php echo set_value('score_option',isset($question->score_option)?$question->score_option:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('score_option'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="question.status" ng-value="question.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  