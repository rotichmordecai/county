
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="QuestionaireController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Questionaire</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        
<div class="bs-example" data-example-id="simple-table"> <table class="table"> <caption>Details - Questionaire</caption> <tbody> 
                         
                                    <tr> <th scope="row">id</th> <td> <?php echo $questionaire->id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">title</th> <td> <?php echo $questionaire->title; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">status</th> <td> <?php echo $questionaire->status; ?> </td> </tr>                                    
                              
            </tbody> </table> </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  