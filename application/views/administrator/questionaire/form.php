<script type="text/javascript">
                    ng_app.controller('QuestionaireController', function ($scope) {
        $scope.questionaire = {
        
            id: '<?php echo set_value('id',isset($questionaire->id)?$questionaire->id:''); ?>',
            title: '<?php echo set_value('title',isset($questionaire->title)?$questionaire->title:''); ?>',
            created: '<?php echo set_value('created',isset($questionaire->created)?$questionaire->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($questionaire->updated)?$questionaire->updated:''); ?>',
            status: '<?php echo set_value('status',isset($questionaire->status)?$questionaire->status:''); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="QuestionaireController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Questionaire</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'questionaire-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                                <label>title</label>
                                <input class="form-control" id="title" name="title" ng-model="questionaire.title" ng-value="questionaire.title" value="<?php echo set_value('title',isset($questionaire->title)?$questionaire->title:''); ?>"  placeholder="Title">
                                <p class="help-block"><?php echo form_error('title'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="questionaire.status" ng-value="questionaire.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  