<script type="text/javascript">
                    ng_app.controller('ResponseController', function ($scope) {
        $scope.response = {
        
            id: '<?php echo set_value('id',isset($response->id)?$response->id:''); ?>',
            county_id: '<?php echo set_value('county_id',isset($response->county_id)?$response->county_id:''); ?>',
            quarter_id: '<?php echo set_value('quarter_id',isset($response->quarter_id)?$response->quarter_id:''); ?>',
            questionaire_id: '<?php echo set_value('questionaire_id',isset($response->questionaire_id)?$response->questionaire_id:''); ?>',
            response: '<?php echo set_value('response',isset($response->response)?$response->response:''); ?>',
            created: '<?php echo set_value('created',isset($response->created)?$response->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($response->updated)?$response->updated:''); ?>',
            status: '<?php echo set_value('status',isset($response->status)?$response->status:''); ?>',
            county: '<?php echo json_encode($county); ?>',
            quarter: '<?php echo json_encode($quarter); ?>',
            questionaire: '<?php echo json_encode($questionaire); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="ResponseController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Response</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'response-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('county_id')) > 0 ? 'has-error' : '' ?>">
                                <label>county_id</label>
                                <select id="county_id" name="county_id" ng-model="response.county_id" ng-value="response.county_id">
                                    <?php foreach ($select_county as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('county_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('quarter_id')) > 0 ? 'has-error' : '' ?>">
                                <label>quarter_id</label>
                                <select id="quarter_id" name="quarter_id" ng-model="response.quarter_id" ng-value="response.quarter_id">
                                    <?php foreach ($select_quarter as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('quarter_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('questionaire_id')) > 0 ? 'has-error' : '' ?>">
                                <label>questionaire_id</label>
                                <select id="questionaire_id" name="questionaire_id" ng-model="response.questionaire_id" ng-value="response.questionaire_id">
                                    <?php foreach ($select_questionaire as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('questionaire_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('response')) > 0 ? 'has-error' : '' ?>">
                                <label>response</label>
                                <textarea class="form-control" id="response" name="response" ng-model="response.response" ng-value="response.response"><?php echo set_value('response',isset($response->response)?$response->response:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('response'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="response.status" ng-value="response.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  