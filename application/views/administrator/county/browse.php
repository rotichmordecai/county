 
            
        <div class="row">
                <div id="breadcrumb" class="col-md-12">
                        <?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
                </div>
        </div>
        <?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
        <div class="row" ng-controller="CountyController">
	<div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-linux"></i>
                    <span>Browse County</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding table-responsive">
            <br />
        
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="http://localhost/county/index.php/administrator/county/add" class="btn btn-sm btn-default pull-right"><span class="glyphicon glyphicon-plus"></span> Add</a>
            </div>
        </div>
                        
        <table id="county_datatable" class="table table-bordered table-striped table-hover table-heading table-datatable" cellspacing="0" width="100%">
        <thead>
            <tr><th>Id</th>
            <th>Name</th>
            <th>Status</th>
               
        <th>Action</th>        
        </tr>
        </thead>
        <tfoot>
        <tr><th>Id</th>
            <th>Name</th>
            <th>Status</th>
               
        <th>Action</th>    
        </tr>
        </tfoot>
    </table>
    </div>
    </div>
    </div>
    </div>
    
        
        <script type="text/javascript">
            $(document).ready(function () {

                $('#county_datatable').dataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "http://localhost/county/index.php/administrator/county/browse_json",
                        "type": "POST"
                    },
                    "columns": [
                    {"data": "id"},
            {"data": "name"},
            {"data": "status"},
            {"data": "action"}      ],
            "aaSorting": [[0, "asc"]],
            "sDom": "T<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": '_MENU_'
            },
            "oTableTools": {
                "sSwfPath": "http://localhost/county/index.php/assets/plugins/datatables/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "copy",
                    "print",
                    {
                        "sExtends": "collection",
                        "sButtonText": 'Save <span class="caret" />',
                        "aButtons": ["csv", "xls", "pdf"]
                    }
                ]
            }
                });

            });
        </script>
        
        