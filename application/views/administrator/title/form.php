<script type="text/javascript">
                    ng_app.controller('TitleController', function ($scope) {
        $scope.title = {
        
            id: '<?php echo set_value('id',isset($title->id)?$title->id:''); ?>',
            category_id: '<?php echo set_value('category_id',isset($title->category_id)?$title->category_id:''); ?>',
            title: '<?php echo set_value('title',isset($title->title)?$title->title:''); ?>',
            description: '<?php echo set_value('description',isset($title->description)?$title->description:''); ?>',
            created: '<?php echo set_value('created',isset($title->created)?$title->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($title->updated)?$title->updated:''); ?>',
            status: '<?php echo set_value('status',isset($title->status)?$title->status:''); ?>',
            category: '<?php echo json_encode($category); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="TitleController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Title</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'title-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('category_id')) > 0 ? 'has-error' : '' ?>">
                                <label>category_id</label>
                                <select id="category_id" name="category_id" ng-model="title.category_id" ng-value="title.category_id">
                                    <?php foreach ($select_category as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('category_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                                <label>title</label>
                                <input class="form-control" id="title" name="title" ng-model="title.title" ng-value="title.title" value="<?php echo set_value('title',isset($title->title)?$title->title:''); ?>"  placeholder="Title">
                                <p class="help-block"><?php echo form_error('title'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('description')) > 0 ? 'has-error' : '' ?>">
                                <label>description</label>
                                <textarea class="form-control" id="description" name="description" ng-model="title.description" ng-value="title.description"><?php echo set_value('description',isset($title->description)?$title->description:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('description'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="title.status" ng-value="title.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  