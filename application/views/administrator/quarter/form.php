<script type="text/javascript">
                    ng_app.controller('QuarterController', function ($scope) {
        $scope.quarter = {
        
            id: '<?php echo set_value('id',isset($quarter->id)?$quarter->id:''); ?>',
            title: '<?php echo set_value('title',isset($quarter->title)?$quarter->title:''); ?>',
            created: '<?php echo set_value('created',isset($quarter->created)?$quarter->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($quarter->updated)?$quarter->updated:''); ?>',
            status: '<?php echo set_value('status',isset($quarter->status)?$quarter->status:''); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="QuarterController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Quarter</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'quarter-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                                <label>title</label>
                                <input class="form-control" id="title" name="title" ng-model="quarter.title" ng-value="quarter.title" value="<?php echo set_value('title',isset($quarter->title)?$quarter->title:''); ?>"  placeholder="Title">
                                <p class="help-block"><?php echo form_error('title'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="quarter.status" ng-value="quarter.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  