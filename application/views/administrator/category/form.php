<script type="text/javascript">
                    ng_app.controller('CategoryController', function ($scope) {
        $scope.category = {
        
            id: '<?php echo set_value('id',isset($category->id)?$category->id:''); ?>',
            questionaire_id: '<?php echo set_value('questionaire_id',isset($category->questionaire_id)?$category->questionaire_id:''); ?>',
            title: '<?php echo set_value('title',isset($category->title)?$category->title:''); ?>',
            created: '<?php echo set_value('created',isset($category->created)?$category->created:''); ?>',
            updated: '<?php echo set_value('updated',isset($category->updated)?$category->updated:''); ?>',
            status: '<?php echo set_value('status',isset($category->status)?$category->status:''); ?>',
            questionaire: '<?php echo json_encode($questionaire); ?>',
        };
        });
        </script>
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="CategoryController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Category</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array('class' => '', 'id' => 'category-form', 'role' => 'form');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        <div class="form-group <?php echo strlen(form_error('questionaire_id')) > 0 ? 'has-error' : '' ?>">
                                <label>questionaire_id</label>
                                <select id="questionaire_id" name="questionaire_id" ng-model="category.questionaire_id" ng-value="category.questionaire_id">
                                    <?php foreach ($select_questionaire as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('questionaire_id'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('title')) > 0 ? 'has-error' : '' ?>">
                                <label>title</label>
                                <textarea class="form-control" id="title" name="title" ng-model="category.title" ng-value="category.title"><?php echo set_value('title',isset($category->title)?$category->title:''); ?></textarea>
                                <p class="help-block"><?php echo form_error('title'); ?></p>
                              </div>
                              <div class="form-group <?php echo strlen(form_error('status')) > 0 ? 'has-error' : '' ?>">
                                <label>status</label>
                                <?php 
                                $status_select = array(
                                    ''  => 'Select',
                                    '0'    => 'Not Active',
                                    '1'    => 'Active'
                                ); 
                                ?>
                                <select id="status" name="status"  ng-model="category.status" ng-value="category.status">
                                    <?php foreach ($status_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error('status'); ?></p>
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  