
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:''; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="CategoryController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Category</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        
<div class="bs-example" data-example-id="simple-table"> <table class="table"> <caption>Details - Category</caption> <tbody> 
                         
                                    <tr> <th scope="row">id</th> <td> <?php echo $category->id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">questionaire_id</th> <td> <?php echo $category->questionaire_id; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">title</th> <td> <?php echo $category->title; ?> </td> </tr>                                    
                               
                                    <tr> <th scope="row">status</th> <td> <?php echo $category->status; ?> </td> </tr>                                    
                              
            </tbody> </table> </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  