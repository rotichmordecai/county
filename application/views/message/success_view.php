<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul>
        <?php foreach ($messages as $message): ?>
            <li><?php echo $message ?></li>
        <?php endforeach ?>
    </ul>
</div>