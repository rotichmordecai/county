<div class="alert alert-warning" role="alert">
    <button data-dismiss="alert" class="close" type="button">×</button>
    <ul style="list-style: none; margin:0 0 0 25px;">
        <?php foreach ($messages as $message): ?>
            <li><?php echo $message ?></li>
        <?php endforeach ?>
    </ul>
</div>
