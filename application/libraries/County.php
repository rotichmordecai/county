<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of County
 *
 * @author Admin
 */
class County {

    private $ci;
    private $data;

    public function __construct() {
        $this->ci = & get_instance();

        $this->ci->load->model('response_model');
        $this->ci->load->model('question_model');
        $this->ci->load->model('county_model');
    }

    function _response($response_id) {

        $this->data = array();

        $this->data['response'] = $this->ci->response_model->get_response_by_id($response_id);
        $this->data['question'] = $this->ci->question_model->select();

        $this->data['response'] = $this->data['response']->response;
        $this->data['response'] = json_decode($this->data['response']);

        $result = array();

        foreach ($this->data['response'] as $question_id => $response) {
            foreach ($this->data['question'] as $question) {
                if ($question_id == $question->question_id) {

                    if (!isset($result[$question_id])) {
                        $result[$question_id] = 0;
                    }

                    if (!is_object($response)) {

                        $score_amount = json_decode($question->score_amount);

                        if (isset($score_amount->point)) {
                            if (is_object($score_amount->point)) {
                                $result[$question_id] += $score_amount->point->{$response};
                            } else {
                                $result[$question_id] += $score_amount->point;
                            }
                        } else {
                            $result[$question_id] = $response;
                        }
                    } else {

                        $score_amount = json_decode($question->score_amount);

                        if (isset($score_amount->score)) {
                            foreach (get_object_vars($score_amount->score) as $key1 => $val1) {
                                if (isset($response->{$key1})) {
                                    $result[$question_id] += $val1;
                                }
                            }
                            $val_val = 0;
                            if (isset($score_amount->total)) {
                                foreach (get_object_vars($score_amount->total) as $key1 => $val1) {
                                    if ($result[$question_id] == $key1) {
                                        $val_val = $val1;
                                    }
                                }
                            }
                            $result[$question_id] = $val_val;
                        } else if (isset($score_amount->point)) {
                            foreach (get_object_vars($score_amount->point) as $key1 => $val1) {
                                if (isset($response->{$key1})) {
                                    if (isset($val1->{$response->{$key1}})) {
                                        $result[$question_id] += $val1->{$response->{$key1}};
                                    } else {
                                        $result[$question_id] += $response->{$key1};
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $return = array();
        $return['detail']['county_id'] = $this->data['response']->county_id;
        $return['detail']['questionaire_id'] = $this->data['response']->questionaire_id;
        $return['detail']['quarter_id'] = $this->data['response']->quarter_id;

        $return['measure'] = array();
        $return['perform'] = array();

        foreach ($result as $key1 => $value) {
            if (is_numeric($value)) {
                $return['measure'][$key1] = $value;
            } else {
                $return['perform'][$key1] = $value;
            }
        }

        return $return;
    }

    function response($response_id = NULL) {

        $return = array();

        if ($response_id == NULL) {
            $_response = $this->ci->response_model->select();
            foreach ($_response as $reponse_value) {
                $response = $this->_response($reponse_value->response_id);
                array_push($return, $response);
            };
        } else {
            $response = $this->_response($response_id);
            array_push($return, $response);
        }

        return $return;
    }

}
