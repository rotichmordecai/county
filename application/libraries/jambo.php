<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JamboCI
 *
 * @author rotichmordecai
 */
class Jambo {

    protected $_ci;
    protected $code;
    protected $tablename;
    protected $classname;
    protected $reference;
    protected $join;
    protected $return;
    protected $select;
    protected $exclude = array(
        'created',
        'updated'
    );

    function __construct() {
        $this->code = NULL;
        $this->_ci = & get_instance();
        $this->_ci->load->helper('file');
        $this->_ci->load->helper('jambo');
        $this->return = array();
    }

    function create() {

        $this->_ci->load->model('jambo_model');
        $database = $this->_ci->jambo_model->database();

        $directory = realpath(APPPATH . '/views') . '/administrator';
        if (!file_exists($directory)) {
            if (!mkdir($directory, 0777, true)) {
                die('Failed to administrator folder');
            }
        }

        $reference = array();

        foreach ($database as $tablename) {
            $reference[$tablename] = array();
            $foreign = $this->_ci->jambo_model->get_foreign_keys($tablename);
            foreach ($foreign as $value) {
                array_push($reference[$tablename], array(
                    'TABLE_NAME' => $value->TABLE_NAME,
                    'COLUMN_NAME' => $value->COLUMN_NAME,
                    'REFERENCED_TABLE_NAME' => $value->REFERENCED_TABLE_NAME,
                    'REFERENCED_COLUMN_NAME' => $value->REFERENCED_COLUMN_NAME,
                ));
            }
        }

        $directory = realpath(APPPATH . '/controllers') . '/administrator/';
        if (!file_exists($directory)) {
            if (!mkdir($directory, 0777, true)) {
                die('Failed to administrator folder for controllers');
            }
        }

        $dashboard_controller = $this->create_controller_dashboard($database);
        $dashboard_detail = $this->create_detail_dashboard($database);


        if (!write_file(realpath(APPPATH . '/controllers') . '/administrator/dashboard.php', $dashboard_controller)) {
            array_push($this->return, 'Error! could not create dasbhoard');
        } else {
            @chmod(realpath(APPPATH . '/controllers') . '/administrator/dasbhoard.php', 0777);
            array_push($this->return, 'Success! created dashboard.php');
        }

        if (!write_file(realpath(APPPATH . '/views') . '/administrator/dashboard.php', $dashboard_detail)) {
            array_push($this->return, 'Error! could not create dasbhoard');
        } else {
            @chmod(realpath(APPPATH . '/views') . '/administrator/dasbhoard.php', 0777);
            array_push($this->return, 'Success! created dashboard.php');
        }

        foreach ($database as $tablename) {

            $directory = realpath(APPPATH . '/views') . '/administrator/' . $tablename;
            if (!file_exists($directory)) {
                if (!mkdir($directory, 0777, true)) {
                    die('Failed to administrator folder for ' . $tablename);
                }
            }

            $this->tablename = $tablename;
            $this->classname = ucfirst($tablename);
            $this->reference = create_reference($tablename, $reference);

            $this->join = "";

            $this->select = '
                            $this->db->select(\'' . strtolower($this->tablename) . '.*\');
                            $this->db->select(\'' . strtolower($this->tablename) . '.id as ' . strtolower($this->tablename) . '_id\');
                            ';
            foreach ($this->reference as $value) {
                $value = (object) $value;
                if (!empty($value->REFERENCED_TABLE_NAME)) {
                    if ($value->TABLE_NAME != $value->REFERENCED_TABLE_NAME) {
                        $this->select .= '
                            $this->db->select(\'' . strtolower($value->REFERENCED_TABLE_NAME) . '.*\');
                            $this->db->select(\'' . strtolower($value->REFERENCED_TABLE_NAME) . '.id as ' . strtolower($value->REFERENCED_TABLE_NAME) . '_id\');
                    
                            ';
                        $this->join .= "$" . "this->db->join('$value->REFERENCED_TABLE_NAME', '$value->REFERENCED_TABLE_NAME.$value->REFERENCED_COLUMN_NAME = $value->TABLE_NAME.$value->COLUMN_NAME');
                    ";
                    } else {
                        $this->select .= '
                            $this->db->select(\'' . strtolower($this->tablename) . '.' . strtolower($this->tablename) . '_id as parent_id\');
                            ';
                    }
                }
            }

            $attributes = $this->_ci->jambo_model->get_columns($this->tablename);
            $primary_keys = $this->_ci->jambo_model->get_primary_keys($this->tablename);

            if (count($primary_keys) == 1) {
                $this->keyname = $primary_keys[0]->column_name;
            } else {
                $this->keyname = array();
                foreach ($primary_keys as $primary_key) {
                    array_push($this->keyname, $primary_key->column_name);
                }
            }

            $this->set_class_header();
            $this->set_attributes($attributes);
            $this->set_constructor();
            $this->set_get_and_set('get', $attributes);
            $this->set_get_and_set('set', $attributes);
            $this->set_init($attributes);
            $this->set_select();
            $this->retrieve_by_primary_key();
            $this->insert($attributes);
            $this->delete();
            $this->count();
            $this->fetch();
            $this->count_fetch();
            $this->update($attributes);
            $this->search($attributes);
            $this->count_search($attributes);
            $this->set_class_footer();
            $table_form = $this->create_bootstrap_form3($attributes);
            $table_controller = $this->create_controller($attributes);
            $table_browse = $this->create_browse($attributes);
            $table_detail = $this->create_detail($attributes);


            if (!write_file(realpath(APPPATH . '/models') . '/' . $this->classname . '_model.php', $this->code)) {
                array_push($this->return, 'Error! could not create model model - ' . $this->classname . '_model.php');
            } else {
                chmod(realpath(APPPATH . '/models') . '/' . $this->classname . '_model.php', 0777);
                array_push($this->return, 'Success! created model - ' . $this->classname . '_model.php');
            }

            if (!write_file(realpath(APPPATH . '/controllers') . '/administrator/' . ucfirst(strtolower($this->tablename)) . '.php', $table_controller)) {
                array_push($this->return, 'Error! could not create controller for - ' . ucfirst(strtolower($this->tablename)));
            } else {
                chmod(realpath(APPPATH . '/controllers') . '/administrator/' . ucfirst(strtolower($this->tablename)) . '.php', 0777);
                array_push($this->return, 'Success! created controller - ' . ucfirst(strtolower($this->tablename)) . '.php');
            }

            if (!write_file(realpath(APPPATH . '/views') . '/administrator/' . $this->tablename . '/form.php', $table_form)) {
                array_push($this->return, 'Error! could not create form for - ' . $this->tablename);
            } else {
                chmod(realpath(APPPATH . '/views') . '/administrator/' . $this->tablename . '/form.php', 0777);
                array_push($this->return, 'Success! created form - ' . $this->tablename . '/form.php');
            }

            if (!write_file(realpath(APPPATH . '/views') . '/administrator/' . $this->tablename . '/browse.php', $table_browse)) {
                array_push($this->return, 'Error! could not create browse view - ' . $this->tablename);
            } else {
                chmod(realpath(APPPATH . '/views') . '/administrator/' . $this->tablename . '/browse.php', 0777);
                array_push($this->return, 'Success! created browse view - ' . $this->tablename . '/browse.php');
            }

            if (!write_file(realpath(APPPATH . '/views') . '/administrator/' . $this->tablename . '/detail.php', $table_detail)) {
                array_push($this->return, 'Error! could not create detail view - ' . $this->tablename);
            } else {
                chmod(realpath(APPPATH . '/views') . '/administrator/' . $this->tablename . '/detail.php', 0777);
                array_push($this->return, 'Success! created browse view - ' . $this->tablename . '/detail.php');
            }

            $this->code = NULL;
        }

        return $this->return;
    }

    protected function set_class_header() {

        $date = date("d.m.Y");

        $this->code .= "   
        <?php

        if (!defined('BASEPATH'))
            exit('No direct script access allowed');
            
         
        /*
        * @author rotichmordecai
        * -------------------------------------------------------
        * Class name:    " . $this->classname . "_model
        * Developed on:  $date
        * -------------------------------------------------------
        */


        // **********************
        // Class declaration
        // **********************

         
        class " . $this->classname . "_model extends CI_Model {
 
        ";
    }

    protected function set_class_footer() {
        $this->code .= "   
        }       
        
        /* End of file $this->tablename" . "_model.php */
        /* Location: ./application/models/$this->tablename" . "_model.php */

        ?>
        ";
    }

    protected function set_constructor() {

        $this->code .= "
            
        // ********************************************
        // Constructor Method
        // ********************************************

        function __construct()
        {
          parent::__construct();
        }
        ";
    }

    protected function set_attributes($attributes) {
        foreach ($attributes as $attribute) {
            $this->code .= "
            private $$attribute->Field;   // DataType: $attribute->Type";
        }
    }

    protected function set_get_and_set($type, $attributes) {

        $title = ($type == "get") ? "Getter" : "Setter";
        $type2 = ($type == "get") ? "get" : "set";
        $paren = ($type == "get") ? "()" : "($" . "value)";

        $this->code .= "
        // ********************************************
        // $title Methods
        // ********************************************
        ";

        foreach ($attributes as $attribute) {

            $pieces = explode("_", $attribute->Field);
            $getname = "";
            foreach ($pieces as $piece) {
                $getname .= ucfirst($piece);
            }
            $mname = $type2 . '_' . $attribute->Field . $paren;
            if ($type == "get") {
                $mthis = "return $" . "this->" . $attribute->Field;
            } else {
                $mthis = "$" . "this->" . $attribute->Field . " = $" . "value";
            }
            $this->code .= "
            function $mname
            {
              $mthis;
            }
            ";
        }

        if ($type2 == 'get') {
            $this->code .= "
            function get_attribute($" . "attribute) {
            return $" . "this->{ $" . "attribute };
            }
            ";
        } else {
            $this->code .= "
            function set_attribute($" . "attribute, $" . "value) {
            $" . "this->{ $" . "attribute } = $" . "value;
            }
            ";
        }
    }

    protected function set_init($attributes) {

        $row = "$" . "row";
        $this->code .= "
            
        // ********************************************
        // Init Method
        // ********************************************

        function init($row)
        {        
        ";
        foreach ($attributes as $attribute) {
            $cthis = "$" . "this->" . $attribute->Field . " = $" . "row->" . $attribute->Field;
            $this->code .= "
                  $cthis;";
        }
        $this->code .= "
        }
        ";
    }

    protected function set_select() {

        $this->code .= "
        // **********************
        // Select / Get all $this->tablename
        // **********************

        function select($" . "criteria = null)
        {        
        ";

        $this->code .= $this->select;

        $this->code .= "
        
        if(is_array($" . "criteria)) {
        $" . "this->db->where($" . "criteria);
        }
        $" . "this->db->from('$this->tablename');
        $this->join
        $" . "query = $" . "this->db->get();
        return $" . "query->result();

        }
        ";
    }

    protected function retrieve_by_primary_key() {

        if (!is_array($this->keyname)) {
            $this->code .= "
            // **********************
            // Get $this->tablename by $this->keyname
            // **********************

            function get_$this->tablename" . "_by_id($" . "id)
            {

            $" . "this->db->select('*');
            $" . "this->db->where('$this->keyname', $" . "id);
            $" . "query = $" . "this->db->get('$this->tablename');

            foreach ($" . "query->result() as $" . "$this->tablename)
            {
                $" . "this->init($" . "$this->tablename);
                return $" . "$this->tablename;
            }

            }";
        }
    }

    protected function delete() {

        $this->code .= "
        // **********************
        // Delete $this->tablename
        // **********************

        public function delete($" . "criteria = null)
        {
        
        if($" . "criteria != null) {
        $" . "this->db->delete('$this->tablename', $" . "criteria); 
            return $" . "this->db->affected_rows();
            }";
        if (!is_array($this->keyname)) {
            $this->code .= "else {
            $" . "this->db->where('$this->keyname', $" . "this-> $this->keyname);
            $" . "this->db->delete('$this->tablename');
                return $" . "this->db->affected_rows();
                }
            
            ";
        }

        $this->code .= "return 0;";

        $this->code .= "
        }
        ";
    }

    protected function insert($attributes) {

        $this->code .= "
        // **********************
        // Insert $this->tablename
        // **********************

        function insert()
        {
        
        ";

        foreach ($attributes as $attribute) {
            if ($attribute->Field != $this->keyname) {

                $cthis = 'if (isset($this->' . $attribute->Field . '))';
                $cthis .= '$data[\'' . $attribute->Field . '\'] = ' . "$" . "this->" . $attribute->Field;
                $this->code .= "$cthis;
                      ";
            }
        }

        $this->code .= "
             $" . "this->db->insert('$this->tablename', " . "$" . "data); 
             return $" . "this->db->insert_id();
        }
        ";
    }

    protected function count() {

        $this->code .= "
        // **********************
        // Count records
        // **********************

        function count()
        {            
            $" . "this->db->select('*');
            $" . "this->db->from('$this->tablename');
            $" . "query = $" . "this->db->get();
            return $" . "query->num_rows();

        }";
    }

    protected function fetch() {

        $this->code .= "
            
        // **********************
        // Fetch records
        // **********************

        function fetch($" . "criteria = null, $" . "group_by = null, $" . "order_by = null," . "$" . "limit = null, $" . "start = null)
        {
        
        ";

        $this->code .= $this->select;

        $this->code .= "
        
        
            if ($" . "criteria != null && is_array($" . "criteria)) {
                if (count($" . "criteria)) {
                    foreach ($" . "criteria as $" . "key => $" . "value) {
                        if (is_array($" . "value)) {
                           $" . "this->db->where_in($" . "key, $" . "value);
                        } else {
                            $" . "this->db->where($" . "criteria);
                        }
                    }
                } else {
                $" . "this->db->where($" . "criteria);
            }
        }
        $" . "this->db->from('$this->tablename');
        $this->join
        if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
        
        if( isset($" . "limit) && isset($" . "start) ) {
            $" . "this->db->limit($" . "limit, $" . "start);
        }
        
        $" . "query = $" . "this->db->get();

        if ($" . "query->num_rows() > 0) {
            return $" . "query->result();
        }
        return false;

        }";
    }

    protected function count_fetch() {

        $this->code .= "
        // **********************
        // Count records
        // **********************

        function count_fetch($" . "criteria = null, $" . "group_by = null, $" . "order_by = null)
        {
        
        ";

        $this->code .= $this->select;

        $this->code .= "
        
        
        if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
        $" . "this->db->from('$this->tablename');
        $this->join
        if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
        
        $" . "query = $" . "this->db->get();
        return $" . "query->num_rows();

        }";
    }

    protected function update($attributes) {

        $this->code .= "
            
        // **********************
        // Update $this->tablename
        // **********************

        function update($" . "criteria = null){
        
        ";

        foreach ($attributes as $attribute) {
            if ($attribute->Field != $this->keyname) {
                $cthis = 'if (isset($this->' . $attribute->Field . ')) ';
                $cthis .= '$data[\'' . $attribute->Field . '\'] = ' . "$" . "this->" . $attribute->Field;
                $this->code .= "$cthis;
                      ";
            }
        }

        if (!is_array($this->keyname)) {
            $this->code .= "
            if ($" . "this->$this->keyname > 0) {
            $" . "this->db->where('$this->keyname', $" . "this->$this->keyname);
            $" . "this->db->update('$this->tablename', " . "$" . "data); 
            return $" . "this->db->affected_rows();
            } elseif($" . "criteria != null) {
            $" . "this->db->where($" . "criteria); 
            $" . "this->db->update('$this->tablename', " . "$" . "data); 
            return $" . "this->db->affected_rows();
            } else {
            return 0;
            }";
        } else {
            $this->code .= "
            if($" . "criteria != null) {
            $" . "this->db->where($" . "criteria); 
            $" . "this->db->update('$this->tablename', " . "$" . "data); 
            return $" . "this->db->affected_rows();
            } else {
            return 0;
            }";
        }

        $this->code .= "}
        ";
    }

    protected function search($attributes) {

        $this->code .= "
            
        // **********************
        // Search records
        // **********************

        function search($" . "search, $" . "criteria = null, $" . "group_by = null, $" . "order_by = null, $" . "limit = null, $" . "start = null)
        {
        
        $" . "this->db->select('*');";

        $_count = 0;
        foreach ($attributes as $attribute) {
            $pos = strpos($attribute->Type, 'varchar');
            if ($pos === false) {
                
            } else {
                if ($attribute->Field != $this->keyname) {
                    if ($_count == 0) {
                        $cthis = '$this->db->like(\'' . $attribute->Field . '\', $search);';
                        $this->code .= "$cthis
                        ";
                    } else {
                        $cthis = '$this->db->or_like(\'' . $attribute->Field . '\', $search);';
                        $this->code .= "$cthis
                        ";
                    }
                    $_count++;
                }
            }
        }

        $this->code .= "if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
        $" . "this->db->from('$this->tablename');
        $this->join
        if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
        
        if( isset($" . "limit) && isset($" . "start) ) {
            $" . "this->db->limit($" . "limit, $" . "start);
        }
        
        $" . "query = $" . "this->db->get();

        if ($" . "query->num_rows() > 0) {
            return $" . "query->result();
        }
        return false;

        }";
    }

    protected function count_search($attributes) {

        $this->code .= "
        // **********************
        // Count search records
        // **********************

        function count_search($" . "search, $" . "criteria = null, $" . "group_by = null, $" . "order_by = null)
        {
            
        $" . "this->db->select('*');";

        $_count = 0;
        foreach ($attributes as $attribute) {
            $pos = strpos($attribute->Type, 'varchar');
            if ($pos === false) {
                
            } else {
                if ($attribute->Field != $this->keyname) {
                    if ($_count == 0) {
                        $cthis = '$this->db->like(\'' . $attribute->Field . '\', $search);';
                        $this->code .= "$cthis
                        ";
                    } else {
                        $cthis = '$this->db->or_like(\'' . $attribute->Field . '\', $search);';
                        $this->code .= "$cthis
                        ";
                    }
                    $_count++;
                }
            }
        }

        $this->code .= "if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
        
        $" . "this->db->from('$this->tablename');
        $this->join
        if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
        $" . "query = $" . "this->db->get();
        return $" . "query->num_rows();
        }";
    }

    function create_controller($attributes) {

        $controller = '';

        $controller .= '<?php

        defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');
        
        class ' . ucfirst(strtolower($this->tablename)) . ' extends MY_Controller {
            
            protected $view_path = \'administrator/' . strtolower($this->tablename) . '/\';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library(\'datatables\');
                $this->load->library(\'template\');
                $this->load->library(\'form_validation\');
                $this->load->model(\'' . strtolower($this->tablename) . '_model\');
                $this->template->set_template(\'administrator\');
                $this->initialize(\'administrator\');
            ';

        foreach ($this->reference as $value) {
            $value = (object) $value;
            if (!empty($value->REFERENCED_TABLE_NAME)) {
                $controller .= '$this->load->model(\'' . strtolower($value->REFERENCED_TABLE_NAME) . '_model\'); 
';
            }
        }

        $controller .= '}
            
            ';

        $controller .= 'function index() {
            $this->browse();
        }    
        ';

        $controller .= 'function add() {
            
        $this->breadcrumbs->push(\'Home\', \'/\');
        $this->breadcrumbs->push(\'Administrator\', \'/administrator/dashboard/index\');
        $this->breadcrumbs->push(\'Browse ' . ucfirst(strtolower($this->tablename)) . '\', \'/administrator/' . strtolower($this->tablename) . '/browse\');
        $this->breadcrumbs->push(\'Add\', \'/administrator/' . strtolower($this->tablename) . '/add\');
        $this->data[\'breadcrumbs\'] = $this->breadcrumbs->show();
        
        $this->data[\'' . strtolower($this->tablename) . '\'] = array();
        $this->data[\'action\'] = \'Add\';
    
        ';

        foreach ($this->reference as $value) {
            $value = (object) $value;
            if (!empty($value->REFERENCED_TABLE_NAME)) {
                $controller .= '$this->data[\'select_' . strtolower($value->REFERENCED_TABLE_NAME) . '\'] = $this->' . strtolower($value->REFERENCED_TABLE_NAME) . '_model->select();
                    ';
            }
        }

        foreach ($attributes as $attribute) {
            if ($attribute->Field != $this->keyname) {
                if (!in_array($attribute->Field, $this->exclude)) {
                    $controller .= '$this->form_validation->set_rules(\'' . $attribute->Field . '\', \'' . $attribute->Field . '\', \'trim|xss_clean\');                      
                    ';
                }
            }
        }

        $controller .= '
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view(\'content\', $this->view_path .\'form\', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		';

        foreach ($attributes as $attribute) {
            $controller .= '$this->' . strtolower($this->tablename) . '_model->set_' . $attribute->Field . '(set_value(\'' . $attribute->Field . '\'));
            ';
        }

        $controller .= '		
                    if ($this->' . strtolower($this->tablename) . '_model->insert() > 0) 
                    {
                            $this->message->set(\'success\', \'Success! ' . $this->tablename . ' Added\');
                            redirect(\'/administrator/' . strtolower($this->tablename) . '/browse\');
                    }
                    else
                    {
                    $this->message->set(\'error\', \'Error! ' . $this->tablename . ' could not be added\');
                    }';

        $controller .= '
        }';
        $controller .= '                        
        }';

        $controller .= '
                
                ';

        if (!is_array($this->keyname)) {

            $controller .= 'function detail($' . strtolower($this->tablename) . '_' . $this->keyname . ') {
        
        $this->breadcrumbs->push(\'Home\', \'/\');
        $this->breadcrumbs->push(\'Administrator\', \'/administrator/dashboard/index\');
        $this->breadcrumbs->push(\'Browse ' . ucfirst(strtolower($this->tablename)) . '\', \'/administrator/' . strtolower($this->tablename) . '/browse\');
        $this->breadcrumbs->push(\'Details\', \'/administrator/' . strtolower($this->tablename) . '/detail\');
        $this->data[\'breadcrumbs\'] = $this->breadcrumbs->show();
        
        $this->data[\'' . strtolower($this->tablename) . '\'] = $this->' . strtolower($this->tablename) . '_model->get_' . strtolower($this->tablename) . '_by_id($' . strtolower($this->tablename) . '_' . $this->keyname . ');
                
$this->template->write_view(\'content\', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
        ';

            $controller .= '
            }';

            $controller .= 'function edit($' . strtolower($this->tablename) . '_' . $this->keyname . ') {
        
        $this->breadcrumbs->push(\'Home\', \'/\');
        $this->breadcrumbs->push(\'Administrator\', \'/administrator/dashboard/index\');
        $this->breadcrumbs->push(\'Browse ' . ucfirst(strtolower($this->tablename)) . '\', \'/administrator/' . strtolower($this->tablename) . '/browse\');
        $this->breadcrumbs->push(\'Edit\', \'/administrator/' . strtolower($this->tablename) . '/edit\');
        $this->data[\'breadcrumbs\'] = $this->breadcrumbs->show();
        
        $this->data[\'' . strtolower($this->tablename) . '\'] = $this->' . strtolower($this->tablename) . '_model->get_' . strtolower($this->tablename) . '_by_id($' . strtolower($this->tablename) . '_' . $this->keyname . ');
        $this->data[\'action\'] = \'Edit\';
        

        ';

            foreach ($this->reference as $value) {
                $value = (object) $value;
                if (!empty($value->REFERENCED_TABLE_NAME)) {
                    $controller .= '$this->data[\'select_' . strtolower($value->REFERENCED_TABLE_NAME) . '\'] = $this->' . strtolower($value->REFERENCED_TABLE_NAME) . '_model->select();
                    ';
                }
            }

            foreach ($attributes as $attribute) {
                if ($attribute->Field != $this->keyname) {
                    if (!in_array($attribute->Field, $this->exclude)) {
                        $controller .= '$this->form_validation->set_rules(\'' . $attribute->Field . '\', \'' . $attribute->Field . '\', \'trim|xss_clean\');                      
                    ';
                    }
                }
            }

            $controller .= '
		if ($this->form_validation->run() == FALSE) 
		{
                    $this->template->write_view(\'content\', $this->view_path . \'form\', $this->data);
                    $this->template->render();
		}
		else 
		{
		
		';

            foreach ($attributes as $attribute) {
                if ($attribute->Field == $this->keyname) {
                    $controller .= '$this->' . strtolower($this->tablename) . '_model->set_' . $attribute->Field . '($' . strtolower($this->tablename) . '_' . $this->keyname . ');
                                    ';
                } else {
                    $controller .= '$this->' . strtolower($this->tablename) . '_model->set_' . $attribute->Field . '(set_value(\'' . $attribute->Field . '\'));
                                    ';
                }
            }

            $controller .= '		
                    if ($this->' . strtolower($this->tablename) . '_model->update() > 0) 
                    {
                            $this->message->set(\'success\', \'Success! ' . $this->tablename . ' Updated\');
                            redirect(\'/administrator/' . strtolower($this->tablename) . '/browse\');
                    }
                    else
                    {
                    $this->message->set(\'error\', \'Error! ' . $this->tablename . ' could not be Updated\');
                    }';

            $controller .= '
            }';

            $controller .= '
                }';
        }




        $controller .= '
        
        function browse() {
        
            $this->breadcrumbs->push(\'Home\', \'/\');
            $this->breadcrumbs->push(\'Administrator\', \'/administrator/dashboard/index\');
            $this->breadcrumbs->push(\'Browse ' . ucfirst(strtolower($this->tablename)) . '\', \'/administrator/' . strtolower($this->tablename) . '/browse\');
            $this->data[\'breadcrumbs\'] = $this->breadcrumbs->show();
        
	    $this->template->write_view(\'content\', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
	}
                        
        ';

        $attr = array();
        foreach ($attributes as $attribute) {
            array_push($attr, $attribute->Field);
        }

        $controller .= '
        
        function browse_json() {
        
            $action = \'<div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <a class="btn btn-default" href="' . site_url('/administrator/' . strtolower($this->tablename) . '/detail/$1') . '">View</a>
                            <a class="btn btn-default" href="' . site_url('/administrator/' . strtolower($this->tablename) . '/edit/$1') . '">Edit</a>
                            <a class="btn btn-default" href="' . site_url('/administrator/' . strtolower($this->tablename) . '/delete/$1') . '">Delete</a>
                        </div>\';
                
	    $this->datatables
                ->select(\'' . implode(", ", $attr) . '\')
                ->from(\'' . $this->tablename . '\')
                ->add_column(\'action\', $action, \'id\');      
                
        header(\'Content-Type: application/json\');
        echo $this->datatables->generate();
	}
                        
        ';

        if (!is_array($this->keyname)) {
            $controller .= '
        
        function delete($' . strtolower($this->tablename) . '_' . $this->keyname . ') {
        $this->' . strtolower($this->tablename) . '_model->set_id($' . strtolower($this->tablename) . '_' . $this->keyname . ');                 		
        if ($this->' . strtolower($this->tablename) . '_model->delete() > 0) 
        {
            $this->message->set(\'success\', \'Success! ' . $this->tablename . ' Deleted\');
        }
        else
        {
            $this->message->set(\'error\', \'Error! ' . $this->tablename . ' could not be Deleted\');
        }
        redirect(\'/administrator/' . strtolower($this->tablename) . '/browse\');
        }
                    
        ';
        }

        $controller .= '
            }';

        return $controller;
    }

    function create_controller_dashboard($database) {

        $controller = '';

        $controller .= '<?php

        defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');
        
        class Dashboard extends MY_Controller {
            
            protected $view_path = \'administrator/\';
            protected $data = array();

            function __construct() {
                parent::__construct();
                $this->load->library(\'datatables\');
                $this->load->library(\'template\');
                $this->load->library(\'form_validation\');
                $this->template->set_template(\'administrator\');
                $this->initialize(\'administrator\');
            }
            
            ';

        $controller .= '

function index() {
$this->dashboard();
}

function dashboard() {
                $this->data[\'dashboard\'] = array(\'' . implode("','", $database) . '\');
$this->template->write_view(\'content\', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();        

}';


        $controller .= '
            }';

        return $controller;
    }

    function create_detail_dashboard($database) {

        $detail = '';

        $detail .= '
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:\'\'; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="DashboardController">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> Dashboard</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        
                        <ul> 
                        <?php foreach($dashboard as $board): ?>
                         <li><a href=\'<?php echo site_url(\'/administrator/\'.$board.\'/index\'); ?>\'><?php echo ucfirst(strtolower($board)); ?></a></li>
                        <?php endforeach;?>
                        </ul> 
                        ';

        $detail .= '
                  </div>
                  </div>
                  </div>
                  </div>
                  ';

        return $detail;
    }

    function create_browse($attributes) {

        $browse = '';
        $attr = array();
        foreach ($attributes as $attribute) {
            array_push($attr, $attribute->Field);
        }
        $browse .= ' 
            
        <div class="row">
                <div id="breadcrumb" class="col-md-12">
                        <?php echo isset($breadcrumbs)?$breadcrumbs:\'\'; ?>
                </div>
        </div>
        <?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
        <div class="row" ng-controller="' . ucfirst(strtolower($this->tablename)) . 'Controller">
	<div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-linux"></i>
                    <span>Browse ' . ucfirst(strtolower($this->tablename)) . '</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding table-responsive">
            <br />
        
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="' . site_url('/administrator/' . strtolower($this->tablename) . '/add') . '" class="btn btn-sm btn-default pull-right"><span class="glyphicon glyphicon-plus"></span> Add</a>
            </div>
        </div>
                        
        <table id="' . $this->tablename . '_datatable" class="table table-bordered table-striped table-hover table-heading table-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>';
        foreach ($attr as $value) {
            if (!in_array($value, $this->exclude)) {
                $browse .= '<th>' . ucfirst(strtolower($value)) . '</th>
            ';
            }
        }
        $browse .= '   
        <th>Action</th>        
        </tr>
        </thead>
        <tfoot>
        <tr>';
        foreach ($attr as $value) {
            if (!in_array($value, $this->exclude)) {
                $browse .= '<th>' . ucfirst(strtolower($value)) . '</th>
            ';
            }
        }
        $browse .= '   
        <th>Action</th>    
        </tr>
        </tfoot>
    </table>
    </div>
    </div>
    </div>
    </div>
    ';

        $browse .= '
        
        <script type="text/javascript">
            $(document).ready(function () {

                $(\'#' . $this->tablename . '_datatable\').dataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "' . site_url('/administrator/' . strtolower($this->tablename) . '/browse_json') . '",
                        "type": "POST"
                    },
                    "columns": [
                    ';

        foreach ($attr as $value) {
            if (!in_array($value, $this->exclude)) {
                $browse .= '{"data": "' . $value . '"},
            ';
            }
        }

        $browse .= '{"data": "action"}';

        $browse .= '      ],
            "aaSorting": [[0, "asc"]],
            "sDom": "T<\'box-content\'<\'col-sm-6\'f><\'col-sm-6 text-right\'l><\'clearfix\'>>rt<\'box-content\'<\'col-sm-6\'i><\'col-sm-6 text-right\'p><\'clearfix\'>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": \'_MENU_\'
            },
            "oTableTools": {
                "sSwfPath": "' . site_url('assets/plugins/datatables/copy_csv_xls_pdf.swf') . '",
                "aButtons": [
                    "copy",
                    "print",
                    {
                        "sExtends": "collection",
                        "sButtonText": \'Save <span class="caret" />\',
                        "aButtons": ["csv", "xls", "pdf"]
                    }
                ]
            }
                });

            });
        </script>
        
        ';

        return $browse;
    }

    function create_detail($attributes) {

        $detail = '';

        $_reference = array();
        foreach ($this->reference as $reference) {
            $_reference[$reference['COLUMN_NAME']] = $reference['REFERENCED_TABLE_NAME'];
        }

        $detail .= '
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:\'\'; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="' . ucfirst(strtolower($this->tablename)) . 'Controller">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> ' . ucfirst(strtolower($this->tablename)) . '</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        
<div class="bs-example" data-example-id="simple-table"> <table class="table"> <caption>Details - ' . ucfirst(strtolower($this->tablename)) . '</caption> <tbody> 
                        ';

        foreach ($attributes as $attribute) {
            if (!in_array($attribute->Field, $this->exclude)) {
                $detail .= ' 
                                    <tr> <th scope="row">' . $attribute->Field . '</th> <td> <?php echo $' . strtolower($this->tablename) . '->' . $attribute->Field . '; ?> </td> </tr>                                    
                              ';
            }
        }

        $detail .= '
            </tbody> </table> </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  ';

        return $detail;
    }

    function create_bootstrap_form3($attributes) {


        $form = '';

        $_reference = array();
        foreach ($this->reference as $reference) {
            $_reference[$reference['COLUMN_NAME']] = $reference['REFERENCED_TABLE_NAME'];
        }

        $form .= '<script type="text/javascript">
                    ng_app.controller(\'' . ucfirst(strtolower($this->tablename)) . 'Controller\', function ($scope) {';

        $form .= '
        $scope.' . strtolower($this->tablename) . ' = {
        ';
        foreach ($attributes as $attribute) {
            $form .= '
            ' . $attribute->Field . ': \'<?php echo set_value(\'' . $attribute->Field . '\',isset($' . strtolower($this->tablename) . '->' . $attribute->Field . ')?$' . strtolower($this->tablename) . '->' . $attribute->Field . ':\'\'); ?>\',';
            if (isset($_reference[$attribute->Field]) || array_key_exists($attribute->Field, $_reference)) {
                
            }
        }

        foreach ($attributes as $attribute) {
            if (isset($_reference[$attribute->Field]) || array_key_exists($attribute->Field, $_reference)) {
                $form .= '
            ' . strtolower($_reference[$attribute->Field]) . ': \'<?php echo json_encode($' . strtolower($_reference[$attribute->Field]) . '); ?>\',';
            }
        }
        $form .= '
        };';
        $form .= '
        });
        </script>';


        $form .= '
            
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<?php echo isset($breadcrumbs)?$breadcrumbs:\'\'; ?>
	</div>
</div>
<?php if ( $this->message->display() ) { echo $this->message->display(); } ?>
<div class="row" ng-controller="' . ucfirst(strtolower($this->tablename)) . 'Controller">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span><?php echo $action; ?> ' . ucfirst(strtolower($this->tablename)) . '</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                        <?php 
                        $attributes = array(\'class\' => \'\', \'id\' => \'' . $this->tablename . '-form\', \'role\' => \'form\');
                        echo form_open(current_url(), $attributes); 
                        ?>
                        ';

        foreach ($attributes as $attribute) {
            if (!in_array($attribute->Field, $this->exclude)) {
                if ($attribute->Key != 'PRI') {

                    if (isset($_reference[$attribute->Field]) || array_key_exists($attribute->Field, $_reference)) {
                        $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $attribute->Field . '\')) > 0 ? \'has-error\' : \'\' ?>">
                                <label>' . $attribute->Field . '</label>
                                <select id="' . $attribute->Field . '" name="' . $attribute->Field . '" ng-model="' . strtolower($this->tablename) . '.' . $attribute->Field . '" ng-value="' . strtolower($this->tablename) . '.' . $attribute->Field . '">
                                    <?php foreach ($select_' . $_reference[$attribute->Field] . ' as $value): ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->id; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error(\'' . $attribute->Field . '\'); ?></p>
                              </div>
                              ';
                    } else if ($attribute->Type == 'tinyint(1)') {
                        $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $attribute->Field . '\')) > 0 ? \'has-error\' : \'\' ?>">
                                <label>' . $attribute->Field . '</label>
                                <?php 
                                $' . $attribute->Field . '_select = array(
                                    \'\'  => \'Select\',
                                    \'0\'    => \'Not Active\',
                                    \'1\'    => \'Active\'
                                ); 
                                ?>
                                <select id="' . $attribute->Field . '" name="' . $attribute->Field . '"  ng-model="' . strtolower($this->tablename) . '.' . $attribute->Field . '" ng-value="' . strtolower($this->tablename) . '.' . $attribute->Field . '">
                                    <?php foreach ($' . $attribute->Field . '_select as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="help-block"><?php echo form_error(\'' . $attribute->Field . '\'); ?></p>
                              </div>
                              ';
                    } else if (strpos($attribute->Type, 'varchar') !== false) {

                        preg_match_all('!\d+!', $attribute->Type, $digits);

                        if ($digits[0][0] > 150) {
                            $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $attribute->Field . '\')) > 0 ? \'has-error\' : \'\' ?>">
                                <label>' . $attribute->Field . '</label>
                                <textarea class="form-control" id="' . $attribute->Field . '" name="' . $attribute->Field . '" ng-model="' . strtolower($this->tablename) . '.' . $attribute->Field . '" ng-value="' . strtolower($this->tablename) . '.' . $attribute->Field . '"><?php echo set_value(\'' . $attribute->Field . '\',isset($' . strtolower($this->tablename) . '->' . $attribute->Field . ')?$' . strtolower($this->tablename) . '->' . $attribute->Field . ':\'\'); ?></textarea>
                                <p class="help-block"><?php echo form_error(\'' . $attribute->Field . '\'); ?></p>
                              </div>
                              ';
                        } else {
                            $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $attribute->Field . '\')) > 0 ? \'has-error\' : \'\' ?>">
                                <label>' . $attribute->Field . '</label>
                                <input class="form-control" id="' . $attribute->Field . '" name="' . $attribute->Field . '" ng-model="' . strtolower($this->tablename) . '.' . $attribute->Field . '" ng-value="' . strtolower($this->tablename) . '.' . $attribute->Field . '" value="<?php echo set_value(\'' . $attribute->Field . '\',isset($' . strtolower($this->tablename) . '->' . $attribute->Field . ')?$' . strtolower($this->tablename) . '->' . $attribute->Field . ':\'\'); ?>"  placeholder="' . ucfirst(strtolower($attribute->Field)) . '">
                                <p class="help-block"><?php echo form_error(\'' . $attribute->Field . '\'); ?></p>
                              </div>
                              ';
                        }
                    } else if (strpos($attribute->Type, 'text') !== false) {

                        $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $attribute->Field . '\')) > 0 ? \'has-error\' : \'\' ?>">
                                <label>' . $attribute->Field . '</label>
                                <textarea class="form-control" id="' . $attribute->Field . '" name="' . $attribute->Field . '" ng-model="' . strtolower($this->tablename) . '.' . $attribute->Field . '" ng-value="' . strtolower($this->tablename) . '.' . $attribute->Field . '"><?php echo set_value(\'' . $attribute->Field . '\',isset($' . strtolower($this->tablename) . '->' . $attribute->Field . ')?$' . strtolower($this->tablename) . '->' . $attribute->Field . ':\'\'); ?></textarea>
                                <p class="help-block"><?php echo form_error(\'' . $attribute->Field . '\'); ?></p>
                              </div>
                              ';
                    } else {
                        $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $attribute->Field . '\')) > 0 ? \'has-error\' : \'\' ?>">
                                <label>' . $attribute->Field . '</label>
                                <input class="form-control" id="' . $attribute->Field . '" name="' . $attribute->Field . '" ng-model="' . strtolower($this->tablename) . '.' . $attribute->Field . '" ng-value="' . strtolower($this->tablename) . '.' . $attribute->Field . '" value="<?php echo set_value(\'' . $attribute->Field . '\',isset($' . strtolower($this->tablename) . '->' . $attribute->Field . ')?$' . strtolower($this->tablename) . '->' . $attribute->Field . ':\'\'); ?>"  placeholder="' . ucfirst(strtolower($attribute->Field)) . '">
                                <p class="help-block"><?php echo form_error(\'' . $attribute->Field . '\'); ?></p>
                              </div>
                              ';
                    }
                }
            }
        }

        $form .= '<button type="submit" class="btn btn-default">Submit Button</button> 
                  <button type="reset" class="btn btn-default">Reset Button</button>
                  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  </div>
                  ';

        return $form;
        echo $form;
    }

}
