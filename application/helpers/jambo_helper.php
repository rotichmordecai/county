<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if (!function_exists('create_reference')) {

    function create_reference($key, $value) {
        $return = array();
        $column = array();
        foreach ($value[$key] as $_value) {
            if (!(in_array($_value['TABLE_NAME'] . '.' . $_value['COLUMN_NAME'], $column) || in_array($_value['REFERENCED_COLUMN_NAME'] . '.' . $_value['REFERENCED_COLUMN_NAME'], $column))) {
                if ($_value['TABLE_NAME'] != $_value['REFERENCED_TABLE_NAME']) {
                    array_push($column, $_value['TABLE_NAME'] . '.' . $_value['COLUMN_NAME']);
                    array_push($column, $_value['REFERENCED_COLUMN_NAME'] . '.' . $_value['REFERENCED_COLUMN_NAME']);
                    $return = array_merge($value[$key], create_reference($_value['REFERENCED_TABLE_NAME'], $value));
                }
            }
        }
        return $return;
    }

}