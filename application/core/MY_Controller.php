<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author Admin
 */
class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function initialize($section) {

        switch ($section) {
            case 'administrator':

                $this->template->add_css('assets/plugins/bootstrap/bootstrap.css');
                $this->template->add_css('assets/plugins/jquery-ui/jquery-ui.min.css');
                $this->template->add_css('assets/plugins/fancybox/jquery.fancybox.css');
                $this->template->add_css('assets/plugins/fullcalendar/fullcalendar.css');
                $this->template->add_css('assets/plugins/xcharts/xcharts.min.css');
                $this->template->add_css('assets/plugins/select2/select2.css');
                $this->template->add_css('assets/css/style.css');

                $this->template->add_js('assets/plugins/jquery/jquery-2.1.0.min.js');
                $this->template->add_js('assets/plugins/jquery-ui/jquery-ui.min.js');
                $this->template->add_js('assets/plugins/angular/angular.min.js');
                $this->template->add_js('assets/plugins/bootstrap/bootstrap.min.js');
                $this->template->add_js('assets/plugins/justified-gallery/jquery.justifiedgallery.min.js');
                $this->template->add_js('assets/plugins/tinymce/tinymce.min.js');
                $this->template->add_js('assets/plugins/tinymce/jquery.tinymce.min.js');
                $this->template->add_js('assets/plugins/datatables/jquery.dataTables.min.js');
                $this->template->add_js('assets/plugins/datatables/ZeroClipboard.js');
                $this->template->add_js('assets/plugins/datatables/TableTools.js');
                $this->template->add_js('assets/plugins/datatables/dataTables.bootstrap.js');
                $this->template->add_js('assets/js/devoops.js');

                break;

            default:
                break;
        }
    }

}
